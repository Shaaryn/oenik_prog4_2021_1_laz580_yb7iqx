var namespace_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces =
[
    [ "IEnemyLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_enemy_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_enemy_logic" ],
    [ "IGameSceneLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_game_scene_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_game_scene_logic" ],
    [ "IPlayerLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_player_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_player_logic" ]
];