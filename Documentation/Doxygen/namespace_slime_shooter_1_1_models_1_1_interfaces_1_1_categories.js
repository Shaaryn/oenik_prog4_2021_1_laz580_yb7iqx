var namespace_slime_shooter_1_1_models_1_1_interfaces_1_1_categories =
[
    [ "IDoDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_do_damage.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_do_damage" ],
    [ "IMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object" ],
    [ "INonMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_non_moving_object.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_non_moving_object" ],
    [ "ITakeDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_take_damage.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_take_damage" ]
];