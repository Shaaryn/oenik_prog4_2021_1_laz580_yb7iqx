var annotated_dup =
[
    [ "SlimeShooter", "namespace_slime_shooter.html", [
      [ "Data", "namespace_slime_shooter_1_1_data.html", [
        [ "Entities", "namespace_slime_shooter_1_1_data_1_1_entities.html", [
          [ "Player", "class_slime_shooter_1_1_data_1_1_entities_1_1_player.html", "class_slime_shooter_1_1_data_1_1_entities_1_1_player" ]
        ] ],
        [ "SlimeShooterDbContext", "class_slime_shooter_1_1_data_1_1_slime_shooter_db_context.html", "class_slime_shooter_1_1_data_1_1_slime_shooter_db_context" ]
      ] ],
      [ "Factory", "namespace_slime_shooter_1_1_factory.html", [
        [ "RepoFactory", "class_slime_shooter_1_1_factory_1_1_repo_factory.html", "class_slime_shooter_1_1_factory_1_1_repo_factory" ]
      ] ],
      [ "GameLogic", "namespace_slime_shooter_1_1_game_logic.html", [
        [ "GameScene", "namespace_slime_shooter_1_1_game_logic_1_1_game_scene.html", [
          [ "Helper", "namespace_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper.html", [
            [ "MapGenerator", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_map_generator.html", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_map_generator" ],
            [ "ModelCache", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_model_cache.html", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_model_cache" ]
          ] ],
          [ "PauseLogic", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_pause_logic.html", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_pause_logic" ]
        ] ],
        [ "MainMenuScene", "namespace_slime_shooter_1_1_game_logic_1_1_main_menu_scene.html", [
          [ "Helper", "namespace_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_helper.html", [
            [ "MapperFactory", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_helper_1_1_mapper_factory.html", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_helper_1_1_mapper_factory" ]
          ] ],
          [ "MainMenuSceneLogic", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_main_menu_scene_logic.html", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_main_menu_scene_logic" ]
        ] ],
        [ "Tests", "namespace_slime_shooter_1_1_game_logic_1_1_tests.html", [
          [ "EnemyLogicTest", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_enemy_logic_test.html", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_enemy_logic_test" ],
          [ "PlayerLogicTest", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_player_logic_test.html", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_player_logic_test" ]
        ] ]
      ] ],
      [ "Logic", "namespace_slime_shooter_1_1_logic.html", [
        [ "GameScene", "namespace_slime_shooter_1_1_logic_1_1_game_scene.html", [
          [ "EnemyLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_enemy_logic.html", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_enemy_logic" ],
          [ "GameSceneLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_game_scene_logic.html", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_game_scene_logic" ],
          [ "PlayerLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_player_logic.html", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_player_logic" ]
        ] ],
        [ "Interfaces", "namespace_slime_shooter_1_1_logic_1_1_interfaces.html", [
          [ "GameScene", "namespace_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene.html", [
            [ "Interfaces", "namespace_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces.html", [
              [ "IEnemyLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_enemy_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_enemy_logic" ],
              [ "IGameSceneLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_game_scene_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_game_scene_logic" ],
              [ "IPlayerLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_player_logic.html", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_player_logic" ]
            ] ]
          ] ]
        ] ],
        [ "MainMenuScene", "namespace_slime_shooter_1_1_logic_1_1_main_menu_scene.html", [
          [ "Interfaces", "namespace_slime_shooter_1_1_logic_1_1_main_menu_scene_1_1_interfaces.html", [
            [ "IMainMenuSceneLogic", "interface_slime_shooter_1_1_logic_1_1_main_menu_scene_1_1_interfaces_1_1_i_main_menu_scene_logic.html", "interface_slime_shooter_1_1_logic_1_1_main_menu_scene_1_1_interfaces_1_1_i_main_menu_scene_logic" ]
          ] ]
        ] ]
      ] ],
      [ "Models", "namespace_slime_shooter_1_1_models.html", [
        [ "Interfaces", "namespace_slime_shooter_1_1_models_1_1_interfaces.html", [
          [ "Categories", "namespace_slime_shooter_1_1_models_1_1_interfaces_1_1_categories.html", [
            [ "IDoDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_do_damage.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_do_damage" ],
            [ "IMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object" ],
            [ "INonMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_non_moving_object.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_non_moving_object" ],
            [ "ITakeDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_take_damage.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_take_damage" ]
          ] ],
          [ "IEnemy", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_enemy.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_enemy" ],
          [ "IGameMap", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_map.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_map" ],
          [ "IGameModel", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model" ],
          [ "IGameSettings", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_settings.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_settings" ],
          [ "IHealthBar", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_health_bar.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_health_bar" ],
          [ "INexus", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_nexus.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_nexus" ],
          [ "IPlayer", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player" ],
          [ "IPortal", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_portal.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_portal" ],
          [ "IProjectile", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_projectile.html", null ],
          [ "ITile", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_tile.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_tile" ],
          [ "IWeapon", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_weapon.html", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_weapon" ]
        ] ],
        [ "Models", "namespace_slime_shooter_1_1_models_1_1_models.html", [
          [ "Weapons", "namespace_slime_shooter_1_1_models_1_1_models_1_1_weapons.html", [
            [ "Pistol", "class_slime_shooter_1_1_models_1_1_models_1_1_weapons_1_1_pistol.html", "class_slime_shooter_1_1_models_1_1_models_1_1_weapons_1_1_pistol" ]
          ] ]
        ] ],
        [ "Projectiles", "namespace_slime_shooter_1_1_models_1_1_projectiles.html", [
          [ "PistolBullet", "class_slime_shooter_1_1_models_1_1_projectiles_1_1_pistol_bullet.html", "class_slime_shooter_1_1_models_1_1_projectiles_1_1_pistol_bullet" ]
        ] ],
        [ "Tiles", "namespace_slime_shooter_1_1_models_1_1_tiles.html", [
          [ "DirtTile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_dirt_tile.html", "class_slime_shooter_1_1_models_1_1_tiles_1_1_dirt_tile" ],
          [ "GrassTile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_grass_tile.html", "class_slime_shooter_1_1_models_1_1_tiles_1_1_grass_tile" ],
          [ "Tile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_tile.html", "class_slime_shooter_1_1_models_1_1_tiles_1_1_tile" ]
        ] ],
        [ "GameModel", "class_slime_shooter_1_1_models_1_1_game_model.html", "class_slime_shooter_1_1_models_1_1_game_model" ],
        [ "GameSettings", "class_slime_shooter_1_1_models_1_1_game_settings.html", "class_slime_shooter_1_1_models_1_1_game_settings" ],
        [ "HealthBar", "class_slime_shooter_1_1_models_1_1_health_bar.html", "class_slime_shooter_1_1_models_1_1_health_bar" ],
        [ "Nexus", "class_slime_shooter_1_1_models_1_1_nexus.html", "class_slime_shooter_1_1_models_1_1_nexus" ],
        [ "Player", "class_slime_shooter_1_1_models_1_1_player.html", "class_slime_shooter_1_1_models_1_1_player" ],
        [ "Portal", "class_slime_shooter_1_1_models_1_1_portal.html", "class_slime_shooter_1_1_models_1_1_portal" ],
        [ "Projectile", "class_slime_shooter_1_1_models_1_1_projectile.html", "class_slime_shooter_1_1_models_1_1_projectile" ],
        [ "SlimeMonster", "class_slime_shooter_1_1_models_1_1_slime_monster.html", "class_slime_shooter_1_1_models_1_1_slime_monster" ],
        [ "Weapon", "class_slime_shooter_1_1_models_1_1_weapon.html", "class_slime_shooter_1_1_models_1_1_weapon" ]
      ] ],
      [ "Renderer", "namespace_slime_shooter_1_1_renderer.html", [
        [ "GFX", "namespace_slime_shooter_1_1_renderer_1_1_g_f_x.html", [
          [ "GameInfoDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_game_info_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_game_info_drawer" ],
          [ "HealthBarDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_health_bar_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_health_bar_drawer" ],
          [ "MapDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_map_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_map_drawer" ],
          [ "NexusCrystalDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_crystal_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_crystal_drawer" ],
          [ "NexusDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_drawer" ],
          [ "PlayerDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_player_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_player_drawer" ],
          [ "PortalDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_portal_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_portal_drawer" ],
          [ "ProjectileDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_projectile_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_projectile_drawer" ],
          [ "SlimeMonsterDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_slime_monster_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_slime_monster_drawer" ],
          [ "WeaponDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_weapon_drawer.html", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_weapon_drawer" ]
        ] ],
        [ "Interfaces", "namespace_slime_shooter_1_1_renderer_1_1_interfaces.html", [
          [ "IDraw", "interface_slime_shooter_1_1_renderer_1_1_interfaces_1_1_i_draw.html", "interface_slime_shooter_1_1_renderer_1_1_interfaces_1_1_i_draw" ]
        ] ],
        [ "GameRenderer", "class_slime_shooter_1_1_renderer_1_1_game_renderer.html", "class_slime_shooter_1_1_renderer_1_1_game_renderer" ],
        [ "Assets", "class_slime_shooter_1_1_renderer_1_1_assets.html", "class_slime_shooter_1_1_renderer_1_1_assets" ],
        [ "SpriteSheetHandler", "class_slime_shooter_1_1_renderer_1_1_sprite_sheet_handler.html", "class_slime_shooter_1_1_renderer_1_1_sprite_sheet_handler" ]
      ] ],
      [ "Repositories", "namespace_slime_shooter_1_1_repositories.html", [
        [ "IRepository", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html", "interface_slime_shooter_1_1_repositories_1_1_i_repository" ],
        [ "PlayerRepository", "class_slime_shooter_1_1_repositories_1_1_player_repository.html", "class_slime_shooter_1_1_repositories_1_1_player_repository" ],
        [ "Repository", "class_slime_shooter_1_1_repositories_1_1_repository.html", "class_slime_shooter_1_1_repositories_1_1_repository" ]
      ] ],
      [ "UI", "namespace_slime_shooter_1_1_u_i.html", [
        [ "GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", "class_slime_shooter_1_1_u_i_1_1_game_page" ],
        [ "GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page" ],
        [ "Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", "class_slime_shooter_1_1_u_i_1_1_leaderboard" ],
        [ "MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", "class_slime_shooter_1_1_u_i_1_1_main_menu" ],
        [ "PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", "class_slime_shooter_1_1_u_i_1_1_paused_game_page" ],
        [ "Settings", "class_slime_shooter_1_1_u_i_1_1_settings.html", "class_slime_shooter_1_1_u_i_1_1_settings" ],
        [ "SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", "class_slime_shooter_1_1_u_i_1_1_settings_page" ],
        [ "PageNavigator", "class_slime_shooter_1_1_u_i_1_1_page_navigator.html", "class_slime_shooter_1_1_u_i_1_1_page_navigator" ]
      ] ],
      [ "App", "class_slime_shooter_1_1_app.html", "class_slime_shooter_1_1_app" ],
      [ "GameControl", "class_slime_shooter_1_1_game_control.html", "class_slime_shooter_1_1_game_control" ],
      [ "GameplaySettings", "class_slime_shooter_1_1_gameplay_settings.html", "class_slime_shooter_1_1_gameplay_settings" ],
      [ "MainWindow", "class_slime_shooter_1_1_main_window.html", "class_slime_shooter_1_1_main_window" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];