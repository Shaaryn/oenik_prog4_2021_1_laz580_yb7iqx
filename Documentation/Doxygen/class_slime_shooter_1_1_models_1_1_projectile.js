var class_slime_shooter_1_1_models_1_1_projectile =
[
    [ "Projectile", "class_slime_shooter_1_1_models_1_1_projectile.html#aa83dbcd438fbca81c543019a482d3248", null ],
    [ "CurrentFrameIndex", "class_slime_shooter_1_1_models_1_1_projectile.html#ab7649fd46d599d9b4239debfc113a373", null ],
    [ "Damage", "class_slime_shooter_1_1_models_1_1_projectile.html#ae91b49476b028aeb3baa9dfedffd5629", null ],
    [ "HitBox", "class_slime_shooter_1_1_models_1_1_projectile.html#acbba882c2a1a385d58f9f5f60bd30f8f", null ],
    [ "PositionX", "class_slime_shooter_1_1_models_1_1_projectile.html#aef779c460c27ccaae265941361c4234c", null ],
    [ "PositionY", "class_slime_shooter_1_1_models_1_1_projectile.html#a46d050a2e728539c3dd5d0c697ff115e", null ],
    [ "Speed", "class_slime_shooter_1_1_models_1_1_projectile.html#a72dee50cc5157d653033cc8916c9af83", null ],
    [ "StepX", "class_slime_shooter_1_1_models_1_1_projectile.html#aedfbf05c8338621c2377061af7247a2a", null ],
    [ "StepY", "class_slime_shooter_1_1_models_1_1_projectile.html#a9812fcab59d3ec9d45a0bf5c7be141b6", null ]
];