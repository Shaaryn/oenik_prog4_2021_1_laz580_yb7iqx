var interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player =
[
    [ "AllWeapons", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#a46c2cd151c477ae997f6cb0c31e28f4f", null ],
    [ "CurrentSpriteIndex", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#ab294ebddc961546df3288ca0aa86088c", null ],
    [ "CurrentWeapon", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#ad01e67569157d1de6edc6cc80f468a65", null ],
    [ "DifficultyMultiplier", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#a3b234fd53816e5bcd0e37f8127c084a9", null ],
    [ "MovementState", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#ab3fbd796a21752d7ebbde01f4f78bb7b", null ],
    [ "Name", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#aed31e4c879173ebfebe43b114bdb222d", null ],
    [ "Score", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#a6b152f018b1ec785b65dfe94b935fe96", null ],
    [ "TrackedProjectiles", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html#a7f9a358065a6a5fc26c9a3eb216df720", null ]
];