var class_slime_shooter_1_1_gameplay_settings =
[
    [ "Difficulty", "class_slime_shooter_1_1_gameplay_settings.html#ac8c4ce67b08d66fab8caa0b0805bc361", null ],
    [ "IsRandom", "class_slime_shooter_1_1_gameplay_settings.html#a2adc04ef733f6d61a11157bd956f255e", null ],
    [ "Map", "class_slime_shooter_1_1_gameplay_settings.html#a6e6b7091d813fbc34b966941377f3d96", null ],
    [ "SelectedPlayerId", "class_slime_shooter_1_1_gameplay_settings.html#ac8ad8485850b7aa55cbae36ff16cb45a", null ],
    [ "SelectedPlayerName", "class_slime_shooter_1_1_gameplay_settings.html#a3e354e7923d654890c5e369f8fe30a9b", null ],
    [ "SelectedPlayerScore", "class_slime_shooter_1_1_gameplay_settings.html#ad8ea7c744024c4b8852095a67134cd6c", null ]
];