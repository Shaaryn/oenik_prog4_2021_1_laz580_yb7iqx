var interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object =
[
    [ "HitBox", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#a14575871d5804fb3a15d0d28293f7815", null ],
    [ "PositionX", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#add31a6d1d6092feeef5c8b225f64837f", null ],
    [ "PositionY", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#a98b6df3d5e19e0134386d5622936189b", null ],
    [ "Speed", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#a44e3e211769126343524fea0fcd80161", null ],
    [ "StepX", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#a77ff09e4bfcb0e81317b577a685fcf32", null ],
    [ "StepY", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html#a1be1b9fce5b8a477e9efb77c66810b95", null ]
];