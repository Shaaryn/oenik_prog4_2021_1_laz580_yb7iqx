var class_slime_shooter_1_1_repositories_1_1_repository =
[
    [ "Repository", "class_slime_shooter_1_1_repositories_1_1_repository.html#adf3d800e002098ac4316ea45580d0d01", null ],
    [ "GetAll", "class_slime_shooter_1_1_repositories_1_1_repository.html#a01a5fb68d77fab58391a747005347767", null ],
    [ "GetOne", "class_slime_shooter_1_1_repositories_1_1_repository.html#a4ad531ef12377936745a1021e267c23a", null ],
    [ "Insert", "class_slime_shooter_1_1_repositories_1_1_repository.html#a1f7b2c1057270d8ed2a95d3c1458fb2e", null ],
    [ "Remove", "class_slime_shooter_1_1_repositories_1_1_repository.html#aceeeeaa9d488e6cb3edcf1d1f9321702", null ],
    [ "Update", "class_slime_shooter_1_1_repositories_1_1_repository.html#a3808ad9c6621eae14035dad19ef7050e", null ]
];