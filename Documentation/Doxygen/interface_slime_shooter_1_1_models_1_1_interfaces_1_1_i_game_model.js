var interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model =
[
    [ "EllapsedTime", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#ad958aa6faf6465af4141ded5898b4b32", null ],
    [ "EnemyNumber", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#afd3c4a0e7156b861d19bee9a45be23b2", null ],
    [ "EnemyPowerUp", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#a13fc19af34dde553604c220dfe3370b2", null ],
    [ "EnemyWave", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#ab4d6a0f500e0b51b83bfda0ae088ebd0", null ],
    [ "Map", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#a4e96ebe64ed4e99ef1902212fb8b59c0", null ],
    [ "MillisecondsTillNextWave", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#a86eb421d2e994f8eeb2dbe23a34a58e0", null ],
    [ "Nexus", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#ab215724b8760a86355dfa6387566f02a", null ],
    [ "Player", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#af16351d973d1a89a65087045b7b7afd7", null ],
    [ "Portals", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#aa0b7bdbd521688cb5ab60eb8f222554f", null ],
    [ "SlimeMonsters", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html#ad6f0e53c4707ed8505f97748097d5e44", null ]
];