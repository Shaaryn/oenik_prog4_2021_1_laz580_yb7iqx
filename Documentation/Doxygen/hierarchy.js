var hierarchy =
[
    [ "Application", null, [
      [ "SlimeShooter.App", "class_slime_shooter_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "SlimeShooter.App", "class_slime_shooter_1_1_app.html", null ],
      [ "SlimeShooter.App", "class_slime_shooter_1_1_app.html", null ]
    ] ],
    [ "SlimeShooter.Renderer.Assets", "class_slime_shooter_1_1_renderer_1_1_assets.html", null ],
    [ "DbContext", null, [
      [ "SlimeShooter.Data.SlimeShooterDbContext", "class_slime_shooter_1_1_data_1_1_slime_shooter_db_context.html", null ]
    ] ],
    [ "SlimeShooter.GameLogic.Tests.EnemyLogicTest", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_enemy_logic_test.html", null ],
    [ "FrameworkElement", null, [
      [ "SlimeShooter.GameControl", "class_slime_shooter_1_1_game_control.html", null ]
    ] ],
    [ "SlimeShooter.GameplaySettings", "class_slime_shooter_1_1_gameplay_settings.html", null ],
    [ "SlimeShooter.Renderer.GameRenderer", "class_slime_shooter_1_1_renderer_1_1_game_renderer.html", null ],
    [ "SlimeShooter.Renderer.GFX.HealthBarDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_health_bar_drawer.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "SlimeShooter.MainWindow", "class_slime_shooter_1_1_main_window.html", null ],
      [ "SlimeShooter.MainWindow", "class_slime_shooter_1_1_main_window.html", null ],
      [ "SlimeShooter.UI.GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", null ],
      [ "SlimeShooter.UI.GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", null ],
      [ "SlimeShooter.UI.GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", null ],
      [ "SlimeShooter.UI.GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", null ],
      [ "SlimeShooter.UI.PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", null ],
      [ "SlimeShooter.UI.Settings", "class_slime_shooter_1_1_u_i_1_1_settings.html", null ],
      [ "SlimeShooter.UI.SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", null ],
      [ "SlimeShooter.UI.SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.Categories.IDoDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_do_damage.html", [
      [ "SlimeShooter.Models.Interfaces.IEnemy", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_enemy.html", [
        [ "SlimeShooter.Models.SlimeMonster", "class_slime_shooter_1_1_models_1_1_slime_monster.html", null ]
      ] ],
      [ "SlimeShooter.Models.Interfaces.IProjectile", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_projectile.html", [
        [ "SlimeShooter.Models.Projectile", "class_slime_shooter_1_1_models_1_1_projectile.html", [
          [ "SlimeShooter.Models.Projectiles.PistolBullet", "class_slime_shooter_1_1_models_1_1_projectiles_1_1_pistol_bullet.html", null ]
        ] ]
      ] ]
    ] ],
    [ "SlimeShooter.Renderer.Interfaces.IDraw", "interface_slime_shooter_1_1_renderer_1_1_interfaces_1_1_i_draw.html", [
      [ "SlimeShooter.Renderer.GFX.GameInfoDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_game_info_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.MapDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_map_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.NexusCrystalDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_crystal_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.NexusDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_nexus_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.PlayerDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_player_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.PortalDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_portal_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.ProjectileDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_projectile_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.SlimeMonsterDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_slime_monster_drawer.html", null ],
      [ "SlimeShooter.Renderer.GFX.WeaponDrawer", "class_slime_shooter_1_1_renderer_1_1_g_f_x_1_1_weapon_drawer.html", null ]
    ] ],
    [ "SlimeShooter.Logic.Interfaces.GameScene.Interfaces.IEnemyLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_enemy_logic.html", [
      [ "SlimeShooter.Logic.GameScene.EnemyLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_enemy_logic.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.IGameMap", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_map.html", null ],
    [ "SlimeShooter.Models.Interfaces.IGameModel", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_model.html", [
      [ "SlimeShooter.Models.GameModel", "class_slime_shooter_1_1_models_1_1_game_model.html", null ]
    ] ],
    [ "SlimeShooter.Logic.Interfaces.GameScene.Interfaces.IGameSceneLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_game_scene_logic.html", [
      [ "SlimeShooter.Logic.GameScene.GameSceneLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_game_scene_logic.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.IGameSettings", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_game_settings.html", [
      [ "SlimeShooter.Models.GameSettings", "class_slime_shooter_1_1_models_1_1_game_settings.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.IHealthBar", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_health_bar.html", [
      [ "SlimeShooter.Models.HealthBar", "class_slime_shooter_1_1_models_1_1_health_bar.html", null ]
    ] ],
    [ "SlimeShooter.Logic.MainMenuScene.Interfaces.IMainMenuSceneLogic", "interface_slime_shooter_1_1_logic_1_1_main_menu_scene_1_1_interfaces_1_1_i_main_menu_scene_logic.html", [
      [ "SlimeShooter.GameLogic.MainMenuScene.MainMenuSceneLogic", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_main_menu_scene_logic.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.Categories.IMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_moving_object.html", [
      [ "SlimeShooter.Models.Interfaces.IEnemy", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_enemy.html", null ],
      [ "SlimeShooter.Models.Interfaces.IPlayer", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_player.html", [
        [ "SlimeShooter.Models.Player", "class_slime_shooter_1_1_models_1_1_player.html", null ]
      ] ],
      [ "SlimeShooter.Models.Interfaces.IProjectile", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_projectile.html", null ],
      [ "SlimeShooter.Models.Interfaces.IWeapon", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_weapon.html", [
        [ "SlimeShooter.Models.Weapon", "class_slime_shooter_1_1_models_1_1_weapon.html", [
          [ "SlimeShooter.Models.Models.Weapons.Pistol", "class_slime_shooter_1_1_models_1_1_models_1_1_weapons_1_1_pistol.html", null ]
        ] ]
      ] ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.Categories.INonMovingObject", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_non_moving_object.html", [
      [ "SlimeShooter.Models.Interfaces.INexus", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_nexus.html", [
        [ "SlimeShooter.Models.Nexus", "class_slime_shooter_1_1_models_1_1_nexus.html", null ]
      ] ],
      [ "SlimeShooter.Models.Interfaces.IPortal", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_portal.html", [
        [ "SlimeShooter.Models.Portal", "class_slime_shooter_1_1_models_1_1_portal.html", null ]
      ] ],
      [ "SlimeShooter.Models.Interfaces.ITile", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_tile.html", [
        [ "SlimeShooter.Models.Tiles.Tile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_tile.html", [
          [ "SlimeShooter.Models.Tiles.DirtTile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_dirt_tile.html", null ],
          [ "SlimeShooter.Models.Tiles.GrassTile", "class_slime_shooter_1_1_models_1_1_tiles_1_1_grass_tile.html", null ]
        ] ]
      ] ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "SlimeShooter.Logic.Interfaces.GameScene.Interfaces.IPlayerLogic", "interface_slime_shooter_1_1_logic_1_1_interfaces_1_1_game_scene_1_1_interfaces_1_1_i_player_logic.html", [
      [ "SlimeShooter.Logic.GameScene.PlayerLogic", "class_slime_shooter_1_1_logic_1_1_game_scene_1_1_player_logic.html", null ]
    ] ],
    [ "SlimeShooter.Repositories.IRepository< T >", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html", null ],
    [ "SlimeShooter.Repositories.IRepository< TEntity >", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html", [
      [ "SlimeShooter.Repositories.Repository< TEntity >", "class_slime_shooter_1_1_repositories_1_1_repository.html", null ]
    ] ],
    [ "SlimeShooter.Models.Interfaces.Categories.ITakeDamage", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_categories_1_1_i_take_damage.html", [
      [ "SlimeShooter.Models.Interfaces.IEnemy", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_enemy.html", null ],
      [ "SlimeShooter.Models.Interfaces.INexus", "interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_nexus.html", null ]
    ] ],
    [ "SlimeShooter.GameLogic.GameScene.Helper.MapGenerator", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_map_generator.html", null ],
    [ "SlimeShooter.GameLogic.MainMenuScene.Helper.MapperFactory", "class_slime_shooter_1_1_game_logic_1_1_main_menu_scene_1_1_helper_1_1_mapper_factory.html", null ],
    [ "SlimeShooter.GameLogic.GameScene.Helper.ModelCache", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_model_cache.html", null ],
    [ "System.Windows.Controls.Page", null, [
      [ "SlimeShooter.UI.GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", null ],
      [ "SlimeShooter.UI.GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", null ],
      [ "SlimeShooter.UI.GamePage", "class_slime_shooter_1_1_u_i_1_1_game_page.html", null ],
      [ "SlimeShooter.UI.GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", null ],
      [ "SlimeShooter.UI.GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", null ],
      [ "SlimeShooter.UI.GameplaySettingsPage", "class_slime_shooter_1_1_u_i_1_1_gameplay_settings_page.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.Leaderboard", "class_slime_shooter_1_1_u_i_1_1_leaderboard.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.MainMenu", "class_slime_shooter_1_1_u_i_1_1_main_menu.html", null ],
      [ "SlimeShooter.UI.PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", null ],
      [ "SlimeShooter.UI.PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", null ],
      [ "SlimeShooter.UI.PausedGamePage", "class_slime_shooter_1_1_u_i_1_1_paused_game_page.html", null ],
      [ "SlimeShooter.UI.Settings", "class_slime_shooter_1_1_u_i_1_1_settings.html", null ],
      [ "SlimeShooter.UI.SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", null ],
      [ "SlimeShooter.UI.SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", null ],
      [ "SlimeShooter.UI.SettingsPage", "class_slime_shooter_1_1_u_i_1_1_settings_page.html", null ]
    ] ],
    [ "SlimeShooter.UI.PageNavigator", "class_slime_shooter_1_1_u_i_1_1_page_navigator.html", null ],
    [ "SlimeShooter.GameLogic.GameScene.PauseLogic", "class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_pause_logic.html", null ],
    [ "SlimeShooter.Data.Entities.Player", "class_slime_shooter_1_1_data_1_1_entities_1_1_player.html", null ],
    [ "SlimeShooter.GameLogic.Tests.PlayerLogicTest", "class_slime_shooter_1_1_game_logic_1_1_tests_1_1_player_logic_test.html", null ],
    [ "SlimeShooter.Factory.RepoFactory", "class_slime_shooter_1_1_factory_1_1_repo_factory.html", null ],
    [ "SlimeShooter.Repositories.Repository< Player >", "class_slime_shooter_1_1_repositories_1_1_repository.html", [
      [ "SlimeShooter.Repositories.PlayerRepository", "class_slime_shooter_1_1_repositories_1_1_player_repository.html", null ]
    ] ],
    [ "SlimeShooter.Renderer.SpriteSheetHandler", "class_slime_shooter_1_1_renderer_1_1_sprite_sheet_handler.html", null ],
    [ "System.Windows.Window", null, [
      [ "SlimeShooter.MainWindow", "class_slime_shooter_1_1_main_window.html", null ],
      [ "SlimeShooter.MainWindow", "class_slime_shooter_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "SlimeShooter.MainWindow", "class_slime_shooter_1_1_main_window.html", null ]
    ] ]
];