var searchData=
[
  ['id_509',['Id',['../class_slime_shooter_1_1_data_1_1_entities_1_1_player.html#a9e7101b32785847e7b9caeb8274ac3ca',1,'SlimeShooter.Data.Entities.Player.Id()'],['../class_slime_shooter_1_1_models_1_1_player.html#aa8a4f6b1290537037887a89ae415d3d2',1,'SlimeShooter.Models.Player.Id()']]],
  ['idlecount_510',['IdleCount',['../class_slime_shooter_1_1_game_logic_1_1_game_scene_1_1_helper_1_1_model_cache.html#a3356ce26729e085df1a3568ad1ddedee',1,'SlimeShooter::GameLogic::GameScene::Helper::ModelCache']]],
  ['isgameover_511',['IsGameOver',['../class_slime_shooter_1_1_logic_1_1_game_scene_1_1_game_scene_logic.html#a2ba15cecb7acb22472b3940104709670',1,'SlimeShooter::Logic::GameScene::GameSceneLogic']]],
  ['isingamemode_512',['IsInGameMode',['../class_slime_shooter_1_1_game_control.html#a7de452276ab73dce7385205b3da57aeb',1,'SlimeShooter::GameControl']]],
  ['israndom_513',['IsRandom',['../class_slime_shooter_1_1_gameplay_settings.html#a2adc04ef733f6d61a11157bd956f255e',1,'SlimeShooter::GameplaySettings']]],
  ['issolid_514',['IsSolid',['../interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_portal.html#a7f1abcdf6978eaf800929b69267fc004',1,'SlimeShooter.Models.Interfaces.IPortal.IsSolid()'],['../interface_slime_shooter_1_1_models_1_1_interfaces_1_1_i_tile.html#acad410d09cb3beb2ae4d4ad90878f0fe',1,'SlimeShooter.Models.Interfaces.ITile.IsSolid()'],['../class_slime_shooter_1_1_models_1_1_portal.html#a188c6e254c1d30c28c0793ef92afc191',1,'SlimeShooter.Models.Portal.IsSolid()'],['../class_slime_shooter_1_1_models_1_1_tiles_1_1_tile.html#ac2316bbad45f122fc93ed94765a6ce19',1,'SlimeShooter.Models.Tiles.Tile.IsSolid()']]]
];
