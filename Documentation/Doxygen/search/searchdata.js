var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuwx",
  1: "adeghilmnprstw",
  2: "sx",
  3: "acdefghilmnoprstuw",
  4: "dmprsw",
  5: "bdefhilmnru",
  6: "acdeghilmnprstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties"
};

