var class_slime_shooter_1_1_models_1_1_slime_monster =
[
    [ "SlimeMonster", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a5e8c1fccc7e1b895e7b825fde2efc3e8", null ],
    [ "CurrentFrameIndex", "class_slime_shooter_1_1_models_1_1_slime_monster.html#af0994e1e5baf9bec8f3c47ae5530ccc9", null ],
    [ "CurrentState", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a42818433553c608689840ff08dae7c74", null ],
    [ "Damage", "class_slime_shooter_1_1_models_1_1_slime_monster.html#ab8bb78e33a718d3b97f446a390b6b253", null ],
    [ "HealthBar", "class_slime_shooter_1_1_models_1_1_slime_monster.html#afda5658db7624bcfc4bd987ba5961f70", null ],
    [ "HealtPoints", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a26c016c2943b20c60ffd1cab45fbe29e", null ],
    [ "HitBox", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a4843bf2731fc24eb0e75431ce0f68652", null ],
    [ "MaxHealtPoints", "class_slime_shooter_1_1_models_1_1_slime_monster.html#adfe668a108fbed18f0901cc778f5b42e", null ],
    [ "PositionX", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a9aa8d3521cd00ede0169d92a6e9fe3aa", null ],
    [ "PositionY", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a0ac235775736ad302fd23827e8c96cea", null ],
    [ "Speed", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a5707b7daa3953cee6cfe6e04544f4095", null ],
    [ "StepX", "class_slime_shooter_1_1_models_1_1_slime_monster.html#a166f7678e808077a872dcda6db02354a", null ],
    [ "StepY", "class_slime_shooter_1_1_models_1_1_slime_monster.html#ac82e785ca5c525ed7b9db72d04f4eaab", null ]
];