var interface_slime_shooter_1_1_repositories_1_1_i_repository =
[
    [ "GetAll", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html#a7afdecac31843fc263a770e8bc8f15b3", null ],
    [ "GetOne", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html#afe23d37532c930fea5ddde0ad3ddade4", null ],
    [ "Insert", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html#ac7895a7e472bd595317da7a0929ef8bb", null ],
    [ "Remove", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html#a0f50fee69e4e4c34d3a739c97974811e", null ],
    [ "Update", "interface_slime_shooter_1_1_repositories_1_1_i_repository.html#a266953be390d9bb656ccd1c317400f24", null ]
];