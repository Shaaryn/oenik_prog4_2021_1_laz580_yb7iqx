﻿namespace SlimeShooter.Data.Entities
{
    /// <summary>
    /// Class representing a player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the Id of a player.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Name of a player.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Score of a player.
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// String representation of the object.
        /// </summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return $"Name: {this.Name}, Score: {this.Score}";
        }
    }
}
