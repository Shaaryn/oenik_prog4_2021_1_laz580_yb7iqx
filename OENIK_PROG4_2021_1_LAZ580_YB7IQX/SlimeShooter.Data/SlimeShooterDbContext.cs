﻿namespace SlimeShooter.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using SlimeShooter.Data.Entities;

    /// <summary>
    /// DbContext class if SlimeShooter.
    /// </summary>
    public partial class SlimeShooterDbContext : DbContext
    {
        /// <summary>
        /// Gets or sets the Players DbSet.
        /// </summary>
        public virtual DbSet<Player> Players { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SlimeShooterDbContext"/> class.
        /// </summary>
        public SlimeShooterDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Database configuration.
        /// </summary>
        /// <param name="optionsBuilder">DbContextOptionsBuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\SlimeShooterDb.mdf;Integrated Security=True;MultipleActiveResultSets=True");
            }
        }

        /// <summary>
        /// DbContext model configuration.
        /// </summary>
        /// <param name="modelBuilder">Modelbuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentException("Modelbuilder is null", nameof(modelBuilder));
            }

            modelBuilder.Entity<Player>()
                .ToTable("player")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Player>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("Id");

            modelBuilder.Entity<Player>()
                .Property(p => p.Name)
                .HasMaxLength(50)
                .IsRequired();

            Player p1 = new Player() { Id = 1, Name = "Johnny", Score = 100 };
            Player p2 = new Player() { Id = 2, Name = "Max", Score = 150 };

            modelBuilder.Entity<Player>().HasData(p1, p2);
        }
    }
}
