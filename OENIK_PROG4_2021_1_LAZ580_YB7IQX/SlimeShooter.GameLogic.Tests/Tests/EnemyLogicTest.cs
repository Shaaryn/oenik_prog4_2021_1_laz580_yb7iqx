﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="EnemyLogicTest.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.Tests
{
    using NUnit.Framework;
    using SlimeShooter.Logic.GameScene;
    using SlimeShooter.Logic.Interfaces.GameScene.Interfaces;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Test class of EnemyLogic.
    /// </summary>
    [TestFixture]
    public class EnemyLogicTest
    {
        /// <summary>
        /// Test for generating waves.
        /// </summary>
        [Test]
        public void TestGenerateWave()
        {
            // Arrange
            GameModel gameModel = new GameModel();
            EnemyLogic enemyLogic = new EnemyLogic(gameModel);
            INexus nexus = new Nexus(500, 500, 100);

            // Act
            enemyLogic.Model.Nexus = new Nexus(400, 400, 100);
            enemyLogic.GenerateWave();

            // Assert
            Assert.That(enemyLogic.Model.EnemyNumber, Is.EqualTo(1));
            Assert.That(enemyLogic.Model.EnemyPowerUp, Is.EqualTo(1));
            Assert.That(enemyLogic.Model.EnemyWave, Is.EqualTo(1));
        }

        /// <summary>
        /// Test for enemy damaging the nexus.
        /// </summary>
        [Test]
        public void TestDealDamage()
        {
            // Arrange
            GameModel gameModel = new GameModel();
            IEnemyLogic enemyLogic = new EnemyLogic(gameModel);
            INexus nexus = new Nexus(500, 500, 100);
            IEnemy enemy = new SlimeMonster(100, 100, 1);

            // Act
            enemyLogic.DealDamage(nexus, enemy);

            // Assert
            Assert.That(nexus.HealtPoints, Is.EqualTo(97.5));
        }
    }
}
