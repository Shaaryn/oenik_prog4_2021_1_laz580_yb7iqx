﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PlayerLogicTest.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.Tests
{
    using System.Windows;
    using NUnit.Framework;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.GameLogic.GameScene.Helper;
    using SlimeShooter.Logic.GameScene;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.Models.Weapons;

    /// <summary>
    /// Test class for PlayerLogic.
    /// </summary>
    [TestFixture]
    public class PlayerLogicTest
    {
        /// <summary>
        /// Test for moving the player.
        /// </summary>
        /// <param name="posX">Initial X position of the player.</param>
        /// <param name="posY">Initial Y position of the player.</param>
        /// <param name="expectedPosX">Expected X position of the player.</param>
        /// <param name="expedtedPosY">Expected Y position of the player.</param>
        [Test]
        [TestCase(300, 200, 196.46446609406726d, 196.46446609406726d)]
        [TestCase(-100, -200, 196.46446609406726d, 196.46446609406726d)]
        [TestCase(100, 100, 196.46446609406726d, 196.46446609406726d)]
        public void TestMovePlayer(double posX, double posY, double expectedPosX, double expedtedPosY)
        {
            // Arrange
            IPlayer player = new Player(posX, posY);
            GameModel gameModel = new GameModel();
            ModelCache modelCache = new ModelCache();
            PlayerLogic playerLogic = new PlayerLogic(gameModel, modelCache);
            playerLogic.Model.Nexus = new Nexus(300, 300, 100);

            // Act
            playerLogic.Move(MovementDirection.Forward);

            // Assert
            Assert.That(playerLogic.Model.Player.PositionX, Is.EqualTo(expectedPosX));
            Assert.That(playerLogic.Model.Player.PositionY, Is.EqualTo(expedtedPosY));
        }

        /// <summary>
        /// Test for spawwing weapons.
        /// </summary>
        [Test]
        public void TestSwapWeapon()
        {
            // Arrange
            IPlayer player = new Player(200d, 200d);
            GameModel gameModel = new GameModel();
            ModelCache modelCache = new ModelCache();
            Projectile projectile = new Projectile(200d, 200d, 5, 10);
            PlayerLogic playerLogic = new PlayerLogic(gameModel, modelCache);

            IWeapon weapon1 = new Pistol("Weapon1", 200d, 200d, 5, new Rect(0, 0, 20, 20), projectile);
            IWeapon weapon2 = new Pistol("Weapon2", 200d, 200d, 5, new Rect(0, 0, 20, 20), projectile);
            playerLogic.Model.Player.AllWeapons.Add(weapon1);
            playerLogic.Model.Player.AllWeapons.Add(weapon2);

            // Act
            playerLogic.SwapWeapon(1);

            // Assert
            Assert.That(playerLogic.Model.Player.CurrentWeapon, Is.EqualTo(weapon2));
        }

        /// <summary>
        /// Test for shooting projectiles.
        /// </summary>
        [Test]
        public void TestShoot()
        {
            // Arrange
            IPlayer player = new Player(200d, 200d);
            GameModel gameModel = new GameModel();
            ModelCache modelCache = new ModelCache();
            PlayerLogic playerLogic = new PlayerLogic(gameModel, modelCache);

            // Act
            playerLogic.Shoot();

            // Assert
            Assert.That(playerLogic.Model.Player.TrackedProjectiles.Count, Is.EqualTo(1));
        }
    }
}
