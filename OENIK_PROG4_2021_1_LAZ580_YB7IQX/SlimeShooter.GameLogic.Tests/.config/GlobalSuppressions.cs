﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GlobalSuppressions.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Im used to have ctor after fields and properties. I think its more readable.")]