﻿namespace SlimeShooter.Factory
{
    using Microsoft.EntityFrameworkCore;
    using SlimeShooter.Data;
    using SlimeShooter.Repositories;

    /// <summary>
    /// Static repository factory class.
    /// </summary>
    public static class RepoFactory
    {
        private static DbContext dbContext = new SlimeShooterDbContext();
        private static PlayerRepository playerRepository = new PlayerRepository(dbContext);

        /// <summary>
        /// Gets the current DbContext.
        /// </summary>
        public static DbContext DbContext
        {
            get { return dbContext; }
        }

        /// <summary>
        /// Gets the current Player repo.
        /// </summary>
        public static PlayerRepository PlayerRepository
        {
            get { return playerRepository; }
        }
    }
}
