﻿namespace SlimeShooter.Config
{
    /// <summary>
    /// Basic configurations of elements related to the actual game.
    /// </summary>
    public static class GameConfig
    {
        /// <summary>
        /// X coordinate of the spawn point of the player.
        /// </summary>
        public static readonly double PlayerSpawnPointX = 200;

        /// <summary>
        /// Y coordinate of the spawn point of the player.
        /// </summary>
        public static readonly double PlayerSpawnPointY = 200;
    }
}
