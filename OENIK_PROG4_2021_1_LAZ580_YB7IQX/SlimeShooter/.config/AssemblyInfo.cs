// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="AssemblyInfo.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

using System.Windows;

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None,
    ResourceDictionaryLocation.SourceAssembly)]
[assembly: System.CLSCompliant(false)]