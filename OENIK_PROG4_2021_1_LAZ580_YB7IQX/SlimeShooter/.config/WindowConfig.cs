﻿namespace SlimeShooter.Config
{
    /// <summary>
    /// Basic configurations of the window.
    /// </summary>
    public static class WindowConfig
    {
        /// <summary>
        /// Gets the width of the windows in pixels.
        /// </summary>
        public static readonly int Width = 1280;

        /// <summary>
        /// Gets the height of the window in pixels.
        /// </summary>
        public static readonly int Height = 720;
    }
}
