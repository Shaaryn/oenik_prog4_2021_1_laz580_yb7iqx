﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MainWindow.xaml.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter
{
    using System.Windows;
    using SlimeShooter.UI;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// <inheritdoc/>
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            PageNavigator.ShowMainMenuPage();
        }
    }
}
