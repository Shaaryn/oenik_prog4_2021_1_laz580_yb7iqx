﻿namespace SlimeShooter
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using SlimeShooter.Factory;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.GameLogic.MainMenuScene;
    using SlimeShooter.Logic.GameScene;
    using SlimeShooter.Models;
    using SlimeShooter.Renderer;
    using SlimeShooter.UI;

    /// <summary>
    /// Control class.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private PauseLogic pauseLogic;
        private GameModel model;
        private GameSceneLogic logic;
        private GameRenderer renderer;
        private MainMenuSceneLogic mainMenuSceneLogic;
        private DispatcherTimer timer;
        private bool isAlreadyLoaded;

        /// <summary>
        /// Gets or sets a value indicating whether the control is in game mode or not.
        /// </summary>
        public static bool IsInGameMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whetHer the saved game should be loaded or not.
        /// </summary>
        public bool LoadSavedGame { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Renderer method.
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.Draw(drawingContext);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            IsInGameMode = true;
            if (this.timer != null)
            {
                this.timer.Start();
            }

            if (!this.isAlreadyLoaded)
            {
                this.model = new GameModel();
                if (this.LoadSavedGame)
                {
                    this.mainMenuSceneLogic = new MainMenuSceneLogic(this.model, RepoFactory.PlayerRepository);
                    this.mainMenuSceneLogic.LoadGame();
                }

                this.model.Player.Id = GameplaySettings.SelectedPlayerId;
                this.model.Player.Name = GameplaySettings.SelectedPlayerName;
                if (GameplaySettings.IsRandom)
                {
                    this.model.GameSettings.MapType = Models.States.MapType.Random;
                }
                else
                {
                    this.model.GameSettings.MapType = Models.States.MapType.File;
                }

                this.model.GameSettings.Difficulty = (Models.States.Difficulty)Enum.Parse(typeof(Models.States.Difficulty), GameplaySettings.Difficulty);

                this.logic = new GameSceneLogic(this.model, RepoFactory.PlayerRepository);
                this.pauseLogic = new PauseLogic(this.model);
                this.renderer = new GameRenderer(this.model);

                Window win = Window.GetWindow(this);
                if (win != null)
                {
                    win.KeyDown += this.Win_KeyDown;
                    win.MouseLeftButtonDown += this.Win_MouseLeftButtonDown;

                    this.timer = new DispatcherTimer();
                    this.timer.Interval = TimeSpan.FromMilliseconds(50);
                    this.timer.Tick += this.Timer_Tick;
                    this.timer.Start();
                }
            }

            if (!this.isAlreadyLoaded)
            {
                this.isAlreadyLoaded = true;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.logic.IsGameOver)
            {
                this.timer.Stop();
                PageNavigator.ShowMainMenuPage();
            }

            Point cursor = Mouse.GetPosition(this);

            this.logic.ExecuteTick(cursor.X, cursor.Y);
            this.InvalidateVisual();
        }

        private void Win_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.logic.PlayerLogic.Shoot();
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (!IsInGameMode)
            {
                return;
            }

            switch (e.Key)
            {
                case Key.D:
                    this.logic.PlayerLogic.Move(MovementDirection.Forward);
                    break;
                case Key.A:
                    this.logic.PlayerLogic.Move(MovementDirection.Backwards);
                    break;
                case Key.Escape:
                    if (GameRenderer.EndOfTick)
                    {
                        this.timer.Stop();
                        PageNavigator.ShowPausedGamePage(this.pauseLogic, this.logic);
                    }

                    break;
            }
        }
    }
}
