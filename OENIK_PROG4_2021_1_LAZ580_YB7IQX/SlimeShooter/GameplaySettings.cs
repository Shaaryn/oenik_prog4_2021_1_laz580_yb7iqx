﻿namespace SlimeShooter
{
    /// <summary>
    /// Static settings class.
    /// </summary>
    public static class GameplaySettings
    {
        /// <summary>
        /// Gets or sets the difficulty.
        /// </summary>
        public static string Difficulty { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// </summary>
        public static string Map { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the map generation should be random or not.
        /// </summary>
        public static bool IsRandom { get; set; }

        /// <summary>
        /// Gets or sets the selected player's id.
        /// </summary>
        public static int SelectedPlayerId { get; set; }

        /// <summary>
        /// Gets or sets the selected player's name.
        /// </summary>
        public static string SelectedPlayerName { get; set; }

        /// <summary>
        /// Gets or sets the selected player's score.
        /// </summary>
        public static int SelectedPlayerScore { get; set; }

        static GameplaySettings()
        {
            Difficulty = "Easy";
            Map = "map1.lvl";
            IsRandom = false;
            SelectedPlayerId = 1;
            SelectedPlayerName = "XxX69BuborekHarcos420XxX";
            SelectedPlayerScore = 0;
        }
    }
}
