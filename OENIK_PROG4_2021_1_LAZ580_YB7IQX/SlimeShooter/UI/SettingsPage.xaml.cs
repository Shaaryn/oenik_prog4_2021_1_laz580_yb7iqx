﻿namespace SlimeShooter.UI
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using SlimeShooter.GameLogic.MainMenuScene;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interaction logic for Settings.xaml.
    /// </summary>
    public partial class SettingsPage : Page
    {
        private Dictionary<int, string> players;
        private MainMenuSceneLogic mainMenuSceneLogic;

        /// <summary>
        /// Gets the available difficulties.
        /// </summary>
        public static Array Difficulties
        {
            get { return Enum.GetValues(typeof(Difficulty)); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsPage"/> class.
        /// </summary>
        /// <param name="mainMenuSceneLogic">MainMenuSceneLogic instance.</param>
        public SettingsPage(MainMenuSceneLogic mainMenuSceneLogic)
        {
            if (mainMenuSceneLogic == null)
            {
                throw new ArgumentNullException(nameof(mainMenuSceneLogic), "Argument cannot be null!");
            }

            this.InitializeComponent();
            this.mainMenuSceneLogic = mainMenuSceneLogic;
            this.Loaded += this.SettingsPage_Loaded;
        }

        private void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.cmbDifficulty.ItemsSource = Difficulties;
            this.cmbDifficulty.SelectedIndex = 0;

            this.players = new Dictionary<int, string>();
            this.cmbMap.Items.Add("Map1");
            this.cmbMap.SelectedIndex = 0;
            this.InitPlayerCmb();
        }

        private void SelectButtonClick(object sender, RoutedEventArgs e)
        {
            GameplaySettings.Difficulty = this.cmbDifficulty.Text;
            GameplaySettings.Map = this.cmbMap.Text;
            GameplaySettings.IsRandom = this.chkAutoGenerate.IsChecked ?? false;
            GameplaySettings.SelectedPlayerId = (int)this.cmbPlayer.SelectedValue;
            GameplaySettings.SelectedPlayerName = this.cmbPlayer.Text;
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.txtNewPlayer.Text))
            {
                MessageBox.Show("A player nevet majd a GLS hozza??!?");
                return;
            }

            this.mainMenuSceneLogic.AddNewPlayer(this.txtNewPlayer.Text);
            this.InitPlayerCmb();
        }

        private void BackToMenuButtonClick(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowMainMenuPage();
        }

        private void InitPlayerCmb()
        {
            this.players.Clear();
            foreach (var player in this.mainMenuSceneLogic.FetchLeaderboard())
            {
                this.players.Add(player.Id, player.Name);
            }

            this.cmbPlayer.ItemsSource = this.players;
            this.cmbPlayer.SelectedValuePath = "Key";
            this.cmbPlayer.DisplayMemberPath = "Value";
            this.cmbPlayer.SelectedIndex = 0;
            this.cmbPlayer.Items.Refresh();
        }
    }
}
