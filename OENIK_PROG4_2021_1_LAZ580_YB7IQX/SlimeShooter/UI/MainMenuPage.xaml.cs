﻿namespace SlimeShooter.UI
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainMenu.xaml.
    /// </summary>
    public partial class MainMenu : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            this.InitializeComponent();
        }

        private void PlayButtonClick(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowGamePage();
        }

        private void LeaderboardButtonClick(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowLeaderboardPage();
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void LoadGameButtonClicked(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowGamePageSavedGame();
        }

        private void SettingButtonClicked(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowSettingsPage();
        }
    }
}
