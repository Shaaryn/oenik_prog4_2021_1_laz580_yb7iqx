﻿namespace SlimeShooter.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.Logic.GameScene;

    /// <summary>
    /// Interaction logic for PausedGamePage.xaml.
    /// </summary>
    public partial class PausedGamePage : Page
    {
        private PauseLogic pauseLogic;
        private GameSceneLogic gameSceneLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="PausedGamePage"/> class.
        /// </summary>
        /// <param name="pauseLogic">PauseLogic instance.</param>
        /// /// <param name="gameSceneLogic">GameScene instance.</param>
        public PausedGamePage(PauseLogic pauseLogic, GameSceneLogic gameSceneLogic)
        {
            this.InitializeComponent();
            this.pauseLogic = pauseLogic;
            this.gameSceneLogic = gameSceneLogic;
        }

        private void SaveGameButtonClick(object sender, RoutedEventArgs e)
        {
            this.pauseLogic.SaveGame();
        }

        private void BackToGameButtonClick(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowGamePage();
        }

        private void BackToMenuClick(object sender, RoutedEventArgs e)
        {
            this.gameSceneLogic.SaveScore();
            PageNavigator.ShowMainMenuPage();
        }
    }
}
