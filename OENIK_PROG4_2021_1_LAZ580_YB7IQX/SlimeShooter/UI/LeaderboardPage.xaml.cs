﻿namespace SlimeShooter.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using SlimeShooter.GameLogic.MainMenuScene;

    /// <summary>
    /// Interaction logic for Leaderboard.xaml.
    /// </summary>
    public partial class Leaderboard : Page
    {
        private MainMenuSceneLogic mainMenuSceneLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Leaderboard"/> class.
        /// </summary>
        /// <param name="mainMenuSceneLogic">MainMenuSceneLogic instance.</param>
        public Leaderboard(MainMenuSceneLogic mainMenuSceneLogic)
        {
            this.mainMenuSceneLogic = mainMenuSceneLogic;
            this.InitializeComponent();
            this.Loaded += this.Leaderboard_Loaded;
        }

        private void Leaderboard_Loaded(object sender, RoutedEventArgs e)
        {
            this.lstView.ItemsSource = this.mainMenuSceneLogic.FetchLeaderboard();
        }

        private void BackToMenuButtonClick(object sender, RoutedEventArgs e)
        {
            PageNavigator.ShowMainMenuPage();
        }
    }
}
