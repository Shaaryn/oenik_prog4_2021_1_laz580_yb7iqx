﻿namespace SlimeShooter.UI
{
    using System.Windows;
    using SlimeShooter.Factory;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.GameLogic.MainMenuScene;
    using SlimeShooter.Logic.GameScene;
    using SlimeShooter.Models;

    /// <summary>
    /// Static page navigator class.
    /// </summary>
    public static class PageNavigator
    {
        private static MainMenu mainMenu = new MainMenu();
        private static GamePage gamePage;

        /// <summary>
        /// Shows main menu.
        /// </summary>
        public static void ShowMainMenuPage()
        {
            GameControl.IsInGameMode = false;
            gamePage = null;
            Application.Current.MainWindow.Content = mainMenu;
        }

        /// <summary>
        /// Shows game page.
        /// </summary>
        public static void ShowGamePage()
        {
            GameControl.IsInGameMode = true;
            if (gamePage != null)
            {
                Application.Current.MainWindow.Content = gamePage;
            }
            else
            {
                gamePage = new GamePage(false);
                Application.Current.MainWindow.Content = gamePage;
            }
        }

        /// <summary>
        /// Shows leaderboard page.
        /// </summary>
        public static void ShowLeaderboardPage()
        {
            Application.Current.MainWindow.Content = new Leaderboard(new MainMenuSceneLogic(new GameModel(), RepoFactory.PlayerRepository));
        }

        /// <summary>
        /// Show paused game page.
        /// </summary>
        /// <param name="pauseLogic">PauseLogic instance.</param>
        /// <param name="gameSceneLogic">GameScene instance.</param>
        public static void ShowPausedGamePage(PauseLogic pauseLogic, GameSceneLogic gameSceneLogic)
        {
            GameControl.IsInGameMode = false;
            Application.Current.MainWindow.Content = new PausedGamePage(pauseLogic, gameSceneLogic);
        }

        /// <summary>
        /// Shows settings page.
        /// </summary>
        public static void ShowSettingsPage()
        {
            Application.Current.MainWindow.Content = new SettingsPage(new MainMenuSceneLogic(new GameModel(), RepoFactory.PlayerRepository));
        }

        /// <summary>
        /// Shows game page with saved game data.
        /// </summary>
        public static void ShowGamePageSavedGame()
        {
            gamePage = new GamePage(true);
            Application.Current.MainWindow.Content = gamePage;
        }
    }
}
