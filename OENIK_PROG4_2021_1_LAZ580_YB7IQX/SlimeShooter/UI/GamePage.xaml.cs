﻿namespace SlimeShooter.UI
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for GamePage.xaml.
    /// </summary>
    public partial class GamePage : Page
    {
        /// <summary>
        /// Gets or sets the game control.
        /// </summary>
        public GameControl GameControl { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePage"/> class.
        /// </summary>
        /// <param name="loadSavedGame">Decides if the game initializes with saved data or not.</param>
        public GamePage(bool loadSavedGame)
        {
            this.Resources.Add("LoadSavedGame", loadSavedGame);
            this.InitializeComponent();
        }
    }
}
