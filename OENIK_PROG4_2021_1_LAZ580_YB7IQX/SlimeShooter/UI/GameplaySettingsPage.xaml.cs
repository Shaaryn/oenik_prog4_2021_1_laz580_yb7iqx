﻿namespace SlimeShooter.UI
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for GameplaySettingsPage.xaml.
    /// </summary>
    public partial class GameplaySettingsPage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameplaySettingsPage"/> class.
        /// </summary>
        public GameplaySettingsPage()
        {
            this.InitializeComponent();
        }
    }
}
