﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Repository.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Generic repository.
    /// </summary>
    /// <typeparam name="TEntity">Entity class.</typeparam>
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private DbContext ctx;
        private DbSet<TEntity> set;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">DbContext of the database.</param>
        public Repository(DbContext context)
        {
            this.ctx = context;
            this.set = this.ctx.Set<TEntity>();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the entity instance.</param>
        /// <returns>Entity instance.</returns>
        public TEntity GetOne(int id)
        {
            return this.set.Find(id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>Queryable colelction of the entity instance collection.</returns>
        public IQueryable<TEntity> GetAll()
        {
            return this.set;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="newEntity">Entity instance we want to insert.</param>
        public void Insert(TEntity newEntity)
        {
            this.set.Add(newEntity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="id">Id of the entity instance we want to modify.</param>
        /// <param name="modifiedEntity">Entity instance containing the modified data.</param>
        public void Update(int id, TEntity modifiedEntity)
        {
            this.set.Attach(modifiedEntity);
            this.ctx.Entry(modifiedEntity).State = EntityState.Modified;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="givenEntity">Entity in stance we want to remove.</param>
        public void Remove(TEntity givenEntity)
        {
            this.set.Remove(givenEntity);
            this.ctx.SaveChanges();
        }
    }
}
