﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IRepository.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface of the <see cref="Repository{TEntity}"> Repository </see> class.
    /// Functions defined here should establish a generic repository once implemented.
    /// </summary>
    /// <typeparam name="T">Any entity class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Fetches one entity instance of the given record.
        /// </summary>
        /// <param name="id">Id of the entity instance.</param>
        /// <returns>Entity instance.</returns>
        T GetOne(int id);

        /// <summary>
        /// Fetches all entity instances of the specific entity type.
        /// </summary>
        /// <returns>Queryable colelction of the entity instance collection.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Inserts an entity instance into the specific entity collection (Set - Table).
        /// </summary>
        /// <param name="newEntity">Entity instance we want to insert.</param>
        void Insert(T newEntity);

        /// <summary>
        /// Updates an entity instance with the provided data.
        /// </summary>
        /// <param name="id">Id of the entity instance we want to modify.</param>
        /// <param name="modifiedEntity">Entity instance containing the modified data.</param>
        void Update(int id, T modifiedEntity);

        /// <summary>
        /// Removes an entity instance from the specific entity collection (Set - Table).
        /// </summary>
        /// <param name="givenEntity">Entity in stance we want to remove.</param>
        void Remove(T givenEntity);
    }
}
