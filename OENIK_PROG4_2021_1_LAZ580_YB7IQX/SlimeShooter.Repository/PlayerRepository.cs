﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PlayerRepository.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using SlimeShooter.Data.Entities;

    /// <summary>
    /// Repository for player entities.
    /// </summary>
    public class PlayerRepository : Repository<Player>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRepository"/> class.
        /// </summary>
        /// <param name="context">Context of the database.</param>
        public PlayerRepository(DbContext context)
            : base(context)
        {
        }
    }
}
