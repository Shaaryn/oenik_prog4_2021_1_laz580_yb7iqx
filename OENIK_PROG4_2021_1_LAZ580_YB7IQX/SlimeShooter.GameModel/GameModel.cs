﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GameModel.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Represent the model of the game.
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Nexus Nexus { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public ITile[,] Map { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Collection<IEnemy> SlimeMonsters { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Dictionary<IPortal, bool> Portals { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public IGameSettings GameSettings { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Stopwatch EllapsedTime { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int MillisecondsTillNextWave { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int EnemyWave { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double EnemyPowerUp { get; set; }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int EnemyNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        public GameModel()
        {
            this.Player = new Player(200, 200);
            this.SlimeMonsters = new Collection<IEnemy>();
            this.Portals = new Dictionary<IPortal, bool>();
            this.GameSettings = new GameSettings() { MapType = MapType.File, Difficulty = Difficulty.Hard };
            this.EllapsedTime = new Stopwatch();
            this.MillisecondsTillNextWave = 15000;
            this.EnemyWave = 0;
            this.EnemyPowerUp = 1.0f;
            this.EnemyNumber = 1;
        }
    }
}
