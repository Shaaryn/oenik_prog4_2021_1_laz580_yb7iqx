﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IHealthBar.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    /// <summary>
    /// HealtBar of the Nexus that represents the player's HP.
    /// </summary>
    public interface IHealthBar
    {
        /// <summary>
        /// Gets or sets the X position of the health bar.
        /// </summary>
        double PositionX { get; set; }

        /// <summary>
        /// Gets or sets the Y position of the health bar.
        /// </summary>
        double PositionY { get; set; }

        /// <summary>
        /// Gets or sets the width of the health bar.
        /// </summary>
        double Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the health bar.
        /// </summary>
        double Height { get; set; }
    }
}
