﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IGameModel.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interface of the <see cref="GameModel"/>'s base that will represent the global variables of the game scene.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the <see cref="Player"/> character who is playing the game.
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Nexus"/> that represents the player's base.
        /// </summary>
        public Nexus Nexus { get; set; }

        /// <summary>
        /// Gets or sets the map that the player playes on. Further defined by the <see cref="MapType"/> property.
        /// </summary>
        public ITile[,] Map { get; set; }

        /// <summary>
        /// Gets or sets the collection of the enemies that the player faces.
        /// </summary>
        public Collection<IEnemy> SlimeMonsters { get; set; }

        /// <summary>
        /// Gets or sets the collection of portals from which the enemies will come from. !ONLY APPLICAPBLE WHEN THE MAPTYPE IS RANDOM!.
        /// </summary>
        public Dictionary<IPortal, bool> Portals { get; set; }

        /// <summary>
        /// Gets or sets the stowpatch to count the elapsed seconds. It will help determine the time between the enemy waves.
        /// </summary>
        public Stopwatch EllapsedTime { get; set; }

        /// <summary>
        /// Gets or sets the seconds after which a new wave is innevitably going to spawn. Decreased by the value of the stopwatch.
        /// </summary>
        public int MillisecondsTillNextWave { get; set; }

        /// <summary>
        /// Gets or sets the current wave that the palyer is facing.
        /// </summary>
        public int EnemyWave { get; set; }

        /// <summary>
        /// Gets or sets how much stronger an enemy is relative to its initial strength.
        /// </summary>
        public double EnemyPowerUp { get; set; }

        /// <summary>
        /// Gets or sets the number of spawning enemies.
        /// </summary>
        public int EnemyNumber { get; set; }
    }
}
