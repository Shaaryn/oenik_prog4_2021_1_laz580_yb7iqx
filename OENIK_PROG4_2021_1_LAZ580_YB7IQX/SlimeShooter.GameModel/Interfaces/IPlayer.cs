﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IPlayer.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using System.Collections.ObjectModel;
    using SlimeShooter.Models.Interfaces.Categories;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interface of a Player entity.
    /// </summary>
    public interface IPlayer : IMovingObject
    {
        /// <summary>
        /// Gets or sets the name of the player. Can be set in the main menu.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the score of the player based on the waves suvived and the <see cref="DifficultyMultiplier"> difficulty multipleir </see>.
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// Gets or sets the value of the multiplier based on the choosen difficulty. Difficulty can be set in the main menu.
        /// </summary>
        public float DifficultyMultiplier { get; set; }

        /// <summary>
        /// Gets or sets the currently selected weapon.
        /// </summary>
        public IWeapon CurrentWeapon { get; set; }

        /// <summary>
        /// Gets or sets the currently used sprite index.
        /// </summary>
        public int CurrentSpriteIndex { get; set; }

        /// <summary>
        /// Gets or sets the current movement state.
        /// </summary>
        public PlayerMovementState MovementState { get; set; }

        /// <summary>
        /// Gets or sets the currently selectable weapons from which the player can chose from.
        /// </summary>
        public Collection<IWeapon> AllWeapons { get; set; }

        /// <summary>
        /// Gets the projectiles that the player shoots. We track them and check wether they hit anything.
        /// </summary>
        public Collection<IProjectile> TrackedProjectiles { get; set; }
    }
}
