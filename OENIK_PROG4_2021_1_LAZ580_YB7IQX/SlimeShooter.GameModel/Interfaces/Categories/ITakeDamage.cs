﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="ITakeDamage.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces.Categories
{
    /// <summary>
    /// Interface of an object that is able to take damage, hgence has a HP.
    /// </summary>
    public interface ITakeDamage
    {
        /// <summary>
        /// Gets or sets the healt points of the object.
        /// </summary>
        public double HealtPoints { get; set; }
    }
}
