﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IDoDamage.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces.Categories
{
    /// <summary>
    /// Interface of an object that is able to do damage.
    /// </summary>
    public interface IDoDamage
    {
        /// <summary>
        /// Gets or sets the value of the damage.
        /// </summary>
        public double Damage { get; set; }
    }
}
