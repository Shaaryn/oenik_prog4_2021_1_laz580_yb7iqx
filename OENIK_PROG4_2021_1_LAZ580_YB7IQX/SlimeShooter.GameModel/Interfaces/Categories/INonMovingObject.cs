﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="INonMovingObject.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces.Categories
{
    /// <summary>
    /// Necessary properties of an object that will not change value.
    /// </summary>
    public interface INonMovingObject
    {
        /// <summary>
        /// Gets or sets the X position of the object.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// Gets or sets the Y position of the object.
        /// </summary>
        public double PositionY { get; set; }
    }
}
