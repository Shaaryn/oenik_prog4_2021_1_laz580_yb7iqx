﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IMovingObject.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces.Categories
{
    using System.Windows;

    /// <summary>
    /// Necessary properties of an object that will frequently change value.
    /// </summary>
    public interface IMovingObject
    {
        /// <summary>
        /// Gets or sets the X position of the object.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// Gets or sets the Y position of the object.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// Gets or sets the number of pixels that the object moves on the X plane.
        /// </summary>
        public double StepX { get; set; }

        /// <summary>
        /// Gets or sets the number of pixels that the object moves on the Y plane.
        /// </summary>
        public double StepY { get; set; }

        /// <summary>
        /// Gets or sets the speed (vector length of a step) of the object.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// Gets or sets the hit-box property of the moving object.
        /// </summary>
        public Rect HitBox { get; set; }
    }
}
