﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="ITile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.Interfaces.Categories;

    /// <summary>
    /// Interface of a tile that will build up a game scene.
    /// </summary>
    public interface ITile : INonMovingObject
    {
        /// <summary>
        /// Gets or sets a value indicating whether tile is solid or not, therefore colliding or not.
        /// </summary>
        public bool IsSolid { get; set; }
    }
}
