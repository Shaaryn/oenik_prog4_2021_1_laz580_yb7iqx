﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IWeapon.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.Interfaces.Categories;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interface of a weapon that will shoot projectiles. The weapon itself does not do nor takes any damage.
    /// </summary>
    public interface IWeapon : IMovingObject
    {
        /// <summary>
        /// Gets or sets the name of the weapon.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets or sets the direction the weapon is pointing at.
        /// </summary>
        public WeaponDirectionState DirectionState { get; set; }

        /// <summary>
        /// Gets or sets the projectile of the weapon (appearance, damage).
        /// </summary>
        public IProjectile Projectile { get; set; }
    }
}
