﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IEnemy.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.Interfaces.Categories;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interface of the enemy entity.
    /// </summary>
    public interface IEnemy : IMovingObject, ITakeDamage, IDoDamage
    {
        /// <summary>
        /// Gets or sets the current state of the slime monster.
        /// </summary>
        SlimeMonsterState CurrentState { get; set; }

        /// <summary>
        /// Gets or sets the current frame index.
        /// </summary>
        int CurrentFrameIndex { get; set; }

        /// <summary>
        /// Gets or sets the healthbar.
        /// </summary>
        HealthBar HealthBar { get; set; }

        /// <summary>
        /// The maximum health points an enemy has.
        /// </summary>
        double MaxHealtPoints { get; set; }
    }
}
