﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IGameMap.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    /// <summary>
    /// Map of the current game that consists of x number of tiles based on its size.
    /// </summary>
    public interface IGameMap
    {
        /// <summary>
        /// Gets or sets the width of the game window size.
        /// </summary>
        public int SizeWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the game window size.
        /// </summary>
        public int SizeHeight { get; set; }

        /// <summary>
        /// Gets or sets tiles that are used to build the map.
        /// </summary>
        public ITile[,] Tiles { get; set; }
    }
}
