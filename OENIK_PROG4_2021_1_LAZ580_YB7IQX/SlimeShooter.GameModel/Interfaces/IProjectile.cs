﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IProjectile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.Interfaces.Categories;

    /// <summary>
    /// Interface of the proejctile that a wepon shoots.
    /// </summary>
    public interface IProjectile : IMovingObject, IDoDamage
    {
    }
}
