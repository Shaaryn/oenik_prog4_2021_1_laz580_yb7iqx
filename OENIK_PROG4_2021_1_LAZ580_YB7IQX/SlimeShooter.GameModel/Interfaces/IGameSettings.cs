﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IGameSettings.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.States;

    /// <summary>
    /// Interface of the <see cref="GameSettings"/>'s base that will represent the global settings of the game.
    /// </summary>
    public interface IGameSettings
    {
        /// <summary>
        /// Gets or sets the difficulty chosen by the player in the Main Menu.
        /// </summary>
        public Difficulty Difficulty { get; set; }

        /// <summary>
        /// Gets or sets the map type choosen by the player in the Main Menu.
        /// </summary>
        public MapType MapType { get; set; }
    }
}
