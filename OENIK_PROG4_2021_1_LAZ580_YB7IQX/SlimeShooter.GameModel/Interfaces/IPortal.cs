﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IPortal.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using SlimeShooter.Models.Interfaces.Categories;

    /// <summary>
    /// Interface of a special tile that represents the spawn point of the enemies.
    /// </summary>
    public interface IPortal : INonMovingObject
    {
        /// <summary>
        /// Gets or sets a value indicating whether tile is solid or not, therefore colliding or not.
        /// </summary>
        public bool IsSolid { get; set; }

        /// <summary>
        /// Gets or sets the current frame.
        /// </summary>
        public int CurrentFrameIndex { get; set; }
    }
}
