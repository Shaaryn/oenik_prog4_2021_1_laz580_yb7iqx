﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="INexus.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Interfaces
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces.Categories;

    /// <summary>
    ///  Interface of the <see cref="IPlayer">Player</see>'s base that will represent the HP of the Player.
    /// </summary>
    public interface INexus : INonMovingObject, ITakeDamage
    {
        /// <summary>
        /// Gets or sets the hit-box property of the moving object.
        /// </summary>
        public Rect HitBox { get; set; }
    }
}
