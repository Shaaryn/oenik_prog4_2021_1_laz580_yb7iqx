﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Portal.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Represents a portal that can serve as a spawn point for enemies.
    /// </summary>
    public class Portal : IPortal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Portal"/> class.
        /// </summary>
        /// <param name="isSolid">Determines if the tile is solid or not, therefore colliding or not.</param>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        public Portal(bool isSolid, double positionX, double positionY)
        {
            this.IsSolid = isSolid;
            this.PositionX = positionX;
            this.PositionY = positionY;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public bool IsSolid { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int CurrentFrameIndex { get; set; }
    }
}
