﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PistolBullet.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Projectiles
{
    /// <summary>
    /// Represents a bulelt that is specifically shot from a Pistol.
    /// </summary>
    public class PistolBullet : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PistolBullet"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="damage">Damage of the projectile.</param>
        /// <param name="speed">Speed of the proejctile.</param>
        public PistolBullet(double positionX, double positionY, double damage, double speed)
            : base(positionX, positionY, damage, speed)
        {
        }
    }
}
