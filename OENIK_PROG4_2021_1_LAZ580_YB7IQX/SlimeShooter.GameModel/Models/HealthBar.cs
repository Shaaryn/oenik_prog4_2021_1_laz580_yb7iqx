﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="HealthBar.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Represents the healthbar of the Nexus.
    /// </summary>
    public class HealthBar : IHealthBar
    {
        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthBar"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="width">Width of the healthbar.</param>
        /// <param name="height">Height of the healtbar.</param>
        public HealthBar(double positionX, double positionY, double width, double height)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.Width = width;
            this.Height = height;
        }
    }
}
