﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="DirtTile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Tiles
{
    /// <summary>
    /// Represent a dirt tile.
    /// </summary>
    public class DirtTile : Tile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DirtTile"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        public DirtTile(double positionX, double positionY)
            : base(positionX, positionY, false)
        {
        }
    }
}
