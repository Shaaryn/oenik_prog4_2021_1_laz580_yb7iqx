﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GrassTile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Tiles
{
    /// <summary>
    /// Represents a grass tile.
    /// </summary>
    public class GrassTile : Tile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrassTile"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        public GrassTile(double positionX, double positionY)
            : base(positionX, positionY, false)
        {
        }
    }
}
