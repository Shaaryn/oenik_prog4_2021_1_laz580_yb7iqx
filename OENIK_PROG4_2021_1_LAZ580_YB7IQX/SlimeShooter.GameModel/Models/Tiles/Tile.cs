﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Tile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Tiles
{
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Base class of tiles.
    /// </summary>
    public abstract class Tile : ITile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tile"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="isSolid">Determines if the tile is solid or not, therefore colliding or not.</param>
        protected Tile(double positionX, double positionY, bool isSolid)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.IsSolid = isSolid;
        }

        /// <inheritdoc/>
        public double PositionX { get; set; }

        /// <inheritdoc/>
        public double PositionY { get; set; }

        /// <inheritdoc/>
        public bool IsSolid { get; set; }
    }
}
