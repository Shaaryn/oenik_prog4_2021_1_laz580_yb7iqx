﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="SlimeMonster.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Represents an enemy that can deal damage to the Nexus.
    /// </summary>
    public class SlimeMonster : IEnemy
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SlimeMonster"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="powerUp">Amount of pwoer increase. Determined in the logic.</param>
        public SlimeMonster(double positionX, double positionY, double powerUp)
        {
            this.HealthBar = new HealthBar(positionX, positionY - 15, 48, 10);

            this.PositionX = positionX;
            this.PositionY = positionY;

            this.HealtPoints = 30 * powerUp;
            this.MaxHealtPoints = this.HealtPoints;
            this.Speed = 3;
            this.Damage = 5 * (powerUp / 2);

            this.CurrentFrameIndex = 0;

            Size hitBoxSize = new Size(50, 50);
            Point hitBoxLocation = new Point(positionX, positionY);
            this.HitBox = new Rect(hitBoxLocation, hitBoxSize);
        }

        private double positionX;
        private double positionY;

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX
        {
            get
            {
                return this.positionX;
            }

            set
            {
                this.positionX = value;
                this.HealthBar.PositionX = this.positionX;
            }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY
        {
            get
            {
                return this.positionY;
            }

            set
            {
                this.positionY = value;
                this.HealthBar.PositionY = this.positionY - 15;
            }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double HealtPoints { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Damage { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Rect HitBox { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public SlimeMonsterState CurrentState { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int CurrentFrameIndex { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public HealthBar HealthBar { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double MaxHealtPoints { get; set; }
    }
}
