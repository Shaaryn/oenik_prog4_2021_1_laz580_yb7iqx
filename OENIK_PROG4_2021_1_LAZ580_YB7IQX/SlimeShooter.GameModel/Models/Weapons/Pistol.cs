﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Pistol.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.Models.Weapons
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Represents a weapon that is specifically a pistol.
    /// </summary>
    public class Pistol : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pistol"/> class.
        /// </summary>
        /// <param name="name">Name of the weapon.</param>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="speed">Not used in this case.</param>
        /// <param name="hitBox">Hitbox of the weapon.</param>
        /// <param name="projectile">Projectile that the weapon shoots.</param>
        public Pistol(string name, double positionX, double positionY, double speed, Rect hitBox, IProjectile projectile)
            : base(name, positionX, positionY, speed, hitBox, projectile)
        {
        }
    }
}
