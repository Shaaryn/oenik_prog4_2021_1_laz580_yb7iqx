﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Nexus.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Represents a nexus that represents the user HP.
    /// </summary>
    public class Nexus : INexus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Nexus"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="healtPoints">HealtPoints of the nexus.</param>
        public Nexus(int positionX, int positionY, double healtPoints)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.HealtPoints = healtPoints;

            Size hitBoxSize = new Size(125, 70);
            Point hitBoxLocation = new Point(positionX + 10, positionY + 20);
            this.HitBox = new Rect(hitBoxLocation, hitBoxSize);
            this.HealthBar = new HealthBar(this.PositionX + 20, this.PositionY - 70, 100, 10);
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double HealtPoints { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Rect HitBox { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public HealthBar HealthBar { get; set; }
    }
}
