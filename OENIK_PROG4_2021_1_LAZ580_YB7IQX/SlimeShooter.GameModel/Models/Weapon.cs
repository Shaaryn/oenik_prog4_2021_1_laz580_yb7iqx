﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Weapon.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Represents a weapon that can fire projectiles.
    /// </summary>
    public class Weapon : IWeapon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Weapon"/> class.
        /// </summary>
        /// <param name="name">Name of the weapon.</param>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="speed">Not used in this case.</param>
        /// <param name="hitBox">Hitbox of the weapon.</param>
        /// <param name="projectile">Projectile that the weapon shoots.</param>
        public Weapon(string name, double positionX, double positionY, double speed, Rect hitBox, IProjectile projectile)
        {
            this.Name = name;
            this.Projectile = projectile;
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.HitBox = hitBox;
            this.Speed = speed;
            this.Speed = 0;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public IProjectile Projectile { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Rect HitBox { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public WeaponDirectionState DirectionState { get; set; }

        /// <summary>
        /// Method that is able to differentiate between two instances of this object.
        /// </summary>
        /// <param name="obj">Another instance of <see cref="Weapon"/>.</param>
        /// <returns>Whether the two instances are the same or not.</returns>
        public override bool Equals(object obj)
        {
            return obj is Weapon weapon &&
                   this.Name == weapon.Name;
        }

        /// <summary>
        /// Generates hash code.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
