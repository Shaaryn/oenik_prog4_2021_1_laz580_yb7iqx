﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GameSettings.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Represents the global settings of the game.
    /// </summary>
    public class GameSettings : IGameSettings
    {
        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public MapType MapType { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Difficulty Difficulty { get; set; }
    }
}
