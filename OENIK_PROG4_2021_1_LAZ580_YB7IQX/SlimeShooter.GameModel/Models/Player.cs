﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Player.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Represents the player of the game.
    /// </summary>
    public class Player : IPlayer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        public Player(double positionX, double positionY)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.Speed = 5;

            Size hitBoxSize = new Size(50, 100);
            Point hitBoxLocation = new Point(positionX, positionY);
            this.HitBox = new Rect(hitBoxLocation, hitBoxSize);

            this.CurrentSpriteIndex = 0;
            this.MovementState = PlayerMovementState.MoveRight;
            this.AllWeapons = new Collection<IWeapon>();
            this.TrackedProjectiles = new Collection<IProjectile>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {
        }

        /// <summary>
        /// Get or sets the Id of the currenty player.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public float DifficultyMultiplier { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public IWeapon CurrentWeapon { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Collection<IWeapon> AllWeapons { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Rect HitBox { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Collection<IProjectile> TrackedProjectiles { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public PlayerMovementState MovementState { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int CurrentSpriteIndex { get; set; }

        /// <summary>
        /// String representation of the object.
        /// </summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return $"Name: {this.Name}, Score: {this.Score}";
        }
    }
}
