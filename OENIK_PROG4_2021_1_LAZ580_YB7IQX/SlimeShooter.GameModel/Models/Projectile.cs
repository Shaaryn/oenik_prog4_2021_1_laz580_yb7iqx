﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Projectile.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models
{
    using System.Windows;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Represents a projectile that can deal damage to enemies.
    /// </summary>
    public class Projectile : IProjectile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Projectile"/> class.
        /// </summary>
        /// <param name="positionX">Position on the X axis.</param>
        /// <param name="positionY">Position on the Y axis.</param>
        /// <param name="damage">Damage of the projectile.</param>
        /// <param name="speed">Speed of the proejctile.</param>
        public Projectile(double positionX, double positionY, double damage, double speed)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
            this.Damage = damage;
            this.Speed = speed;

            Size hitBoxSize = new Size(10, 10);
            Point hitBoxLocation = new Point(positionX, positionY);
            this.HitBox = new Rect(hitBoxLocation, hitBoxSize);
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public int CurrentFrameIndex { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Damage { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public Rect HitBox { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepX { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public double StepY { get; set; }
    }
}
