﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="WeaponDirectionState.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Represents the different directions of a weapon.
    /// </summary>
    public enum WeaponDirectionState
    {
        /// <summary>
        /// Pointing left.
        /// </summary>
        Left = 7,

        /// <summary>
        /// Pointing right.
        /// </summary>
        Right = 3,

        /// <summary>
        /// Pointing up.
        /// </summary>
        Up = 1,

        /// <summary>
        /// Pointing down.
        /// </summary>
        Down = 5,

        /// <summary>
        /// Pointing diagonally left and up.
        /// </summary>
        LeftDiagonalUp = 0,

        /// <summary>
        /// Pointing diagonally left and down.
        /// </summary>
        LeftDiagonalDown = 6,

        /// <summary>
        /// Pointing diagonally right and up.
        /// </summary>
        RightDiagonalUp = 2,

        /// <summary>
        /// Pointing diagonally right and down.
        /// </summary>
        RightDiagonalDown = 4,
    }
}
