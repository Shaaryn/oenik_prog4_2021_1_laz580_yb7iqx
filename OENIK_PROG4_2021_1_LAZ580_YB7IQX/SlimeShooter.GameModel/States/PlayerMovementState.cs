﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PlayerMovementState.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Enum representing the possible states of the player.
    /// </summary>
    public enum PlayerMovementState
    {
        /// <summary>
        /// Idle state.
        /// </summary>
        Idle,

        /// <summary>
        /// Moving right state.
        /// </summary>
        MoveRight,

        /// <summary>
        /// Moving ledt state.
        /// </summary>
        MoveLeft,

        /// <summary>
        /// Moving up state.
        /// </summary>
        MoveUp,

        /// <summary>
        /// Moving down state.
        /// </summary>
        MoveDown,

        /// <summary>
        /// Moving diagonally up and right.
        /// </summary>
        MoveRightDiagonalUp,

        /// <summary>
        /// Moving diagonally down and right.
        /// </summary>
        MoveRightDiagonalDown,

        /// <summary>
        /// Moving diagonally up and left.
        /// </summary>
        MoveLeftDiagonalUp,

        /// <summary>
        /// Moving diagonally down and left.
        /// </summary>
        MoveLeftDiagonalDown,
    }
}
