﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="RouteDirection.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Direction in which the route can be continued.
    /// </summary>
    public enum RouteDirection
    {
        /// <summary>
        /// Represents the left option.
        /// </summary>
        Left,

        /// <summary>
        /// Represents the upwards option.
        /// </summary>
        Up,

        /// <summary>
        /// Represents the roght option.
        /// </summary>
        Right,

        /// <summary>
        /// Represents the downwards option.
        /// </summary>
        Down,
    }
}
