﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MovementDirection.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.GameScene
{
    /// <summary>
    /// Direction of the player movement.
    /// </summary>
    public enum MovementDirection
    {
        /// <summary>
        /// Indicates that the player is moving forward by pressing the 'W' key.
        /// </summary>
        Forward,

        /// <summary>
        /// Indicates that the player is moving backwards by pressing the 'S' key.
        /// </summary>
        Backwards,
    }
}
