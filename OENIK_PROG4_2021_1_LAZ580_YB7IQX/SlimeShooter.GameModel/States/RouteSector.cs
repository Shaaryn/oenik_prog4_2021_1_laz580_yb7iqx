﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="RouteSector.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Quarterly sector of the map area.
    /// </summary>
    public enum RouteSector
    {
        /// <summary>
        /// Represents the upper left quarter.
        /// </summary>
        UpLeft,

        /// <summary>
        /// Represents the upper right quarter.
        /// </summary>
        UpRight,

        /// <summary>
        /// Represents the bottom right quarter.
        /// </summary>
        BotRight,

        /// <summary>
        /// Represents the bottom left quarter.
        /// </summary>
        BotLeft,
    }
}
