﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="Difficulty.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Difficulty of the game.
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// Suggested option by Stylecop.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represent the easy difficulty.
        /// </summary>
        Easy = 2,

        /// <summary>
        /// Represents the hard difficulty.
        /// </summary>
        Hard = 3,

        ///// <summary>
        ///// Represents the impossible difficulty.
        ///// </summary>
        // Impossible = 5,
    }
}
