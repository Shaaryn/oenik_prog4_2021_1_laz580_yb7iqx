﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MapType.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Defines the type of map we are using on creation.
    /// </summary>
    public enum MapType
    {
        /// <summary>
        /// Represents the random map generation with portals.
        /// </summary>
        Random,

        /// <summary>
        /// Represents the level file type.
        /// </summary>
        File,
    }
}
