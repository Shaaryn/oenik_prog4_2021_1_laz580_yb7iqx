﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="SlimeMonsterState.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Models.States
{
    /// <summary>
    /// Represent the possible states of a slime monster.
    /// </summary>
    public enum SlimeMonsterState
    {
        /// <summary>
        /// Slime monster is moving forward the nexus.
        /// </summary>
        MovingForward,

        /// <summary>
        /// Slime monster explodes when reaches the nexus.
        /// </summary>
        Explode,
    }
}
