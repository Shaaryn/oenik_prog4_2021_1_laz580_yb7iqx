﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GlobalSuppressions.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Might be changed later.")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Im used to have ctor after fields and properties. I think its more readable.")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "An n x m array will be used always.")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Might be changed later.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "Its there on a lower level:)")]
