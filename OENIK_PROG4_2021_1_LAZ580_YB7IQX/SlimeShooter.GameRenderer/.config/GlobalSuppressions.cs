﻿using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Im used to have ctor after fields and properties. I think its more readable.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No need for header.")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "This semester we don't worry about performance.")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1517:Code should not contain blank lines at start of file", Justification = "Header is not neede.")]
