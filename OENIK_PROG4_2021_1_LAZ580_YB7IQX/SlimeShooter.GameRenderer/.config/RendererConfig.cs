﻿namespace SlimeShooter.Renderer.Config
{
    /// <summary>
    /// Holds config info related to the GameRenderer project.
    /// </summary>
    public static class RendererConfig
    {
        /// <summary>
        /// Returns the relative path of the sprite sheet.
        /// </summary>
        public static readonly string SpriteSheetRelativePath = @".resources\Images\Slime_shooter_sprite_sheet.png";
    }
}
