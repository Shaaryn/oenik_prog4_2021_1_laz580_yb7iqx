﻿namespace SlimeShooter.Renderer
{
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Renderer.GFX;

    /// <summary>
    /// Class that handles rendering.
    /// </summary>
    public class GameRenderer
    {
        private static bool endOfTick;

        private GameModel model;
        private PlayerDrawer playerDrawer;
        private MapDrawer mapDrawer;
        private WeaponDrawer weaponDrawer;
        private ProjectileDrawer projectileDrawer;
        private NexusDrawer nexusDrawer;
        private NexusCrystalDrawer nexusCrystalDrawer;
        private SlimeMonsterDrawer slimeMonsterDrawer;
        private GameInfoDrawer infoDrawer;
        private PortalDrawer portalDrawer;

        /// <summary>
        /// Gets or sets a value indicating whether a tick ended or not.
        /// </summary>
        public static bool EndOfTick
        {
            get { return endOfTick; }
            set { endOfTick = value; }
        }

        private static int ticks;

        /// <summary>
        /// Gets or sets the elapsed ticks.
        /// </summary>
        public static int Ticks
        {
            get { return ticks; }
            set { ticks = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">GameModel instance.</param>
        public GameRenderer(GameModel model)
        {
            Assets.Init();

            this.model = model;
            this.playerDrawer = new PlayerDrawer(this.model);
            this.mapDrawer = new MapDrawer(this.model);
            this.weaponDrawer = new WeaponDrawer(this.model);
            this.projectileDrawer = new ProjectileDrawer(this.model);
            this.nexusDrawer = new NexusDrawer(this.model);
            this.nexusCrystalDrawer = new NexusCrystalDrawer(this.model);
            this.slimeMonsterDrawer = new SlimeMonsterDrawer(this.model);
            this.portalDrawer = new PortalDrawer(this.model);

            this.infoDrawer = new GameInfoDrawer(this.model);
        }

        private GameRenderer()
        {
            ticks = 0;
            EndOfTick = false;
        }

        /// <summary>
        /// Adds game elements to the drawing context.
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            GameRenderer.EndOfTick = false;
            this.mapDrawer.Draw(drawingContext);
            this.nexusDrawer.Draw(drawingContext);
            this.nexusCrystalDrawer.Draw(drawingContext);
            this.slimeMonsterDrawer.Draw(drawingContext);
            this.weaponDrawer.Draw(drawingContext);
            this.playerDrawer.Draw(drawingContext);
            this.projectileDrawer.Draw(drawingContext);
            this.portalDrawer.Draw(drawingContext);

            this.infoDrawer.Draw(drawingContext);

            GameRenderer.Ticks++;
            GameRenderer.EndOfTick = true;
        }
    }
}
