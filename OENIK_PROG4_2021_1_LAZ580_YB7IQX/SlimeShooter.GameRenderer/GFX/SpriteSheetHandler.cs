﻿namespace SlimeShooter.Renderer
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Contains helper methods to work with sprite sheets.
    /// </summary>
    public static class SpriteSheetHandler
    {
        private static BitmapImage spriteSheet;

        /// <summary>
        /// Sets the resource to be used.
        /// </summary>
        /// <param name="relativePath">Relative path of the resource.</param>
        public static void SetResource(string relativePath)
        {
            if (string.IsNullOrEmpty(relativePath))
            {
                throw new ArgumentException("The argument must contain a valid path.", nameof(relativePath));
            }

            spriteSheet = new BitmapImage();
            spriteSheet.BeginInit();
            spriteSheet.UriSource = new Uri(relativePath, UriKind.RelativeOrAbsolute);
            spriteSheet.EndInit();
        }

        /// <summary>
        /// Crops a part of an image.
        /// </summary>
        /// <param name="x">X coordinate of the cropping.</param>
        /// <param name="y">Y coordinate of the cropping.</param>
        /// <param name="width">Width of the cropping.</param>
        /// <param name="height">Height of the cropping.</param>
        /// <param name="isTiled">Sets if the map is tiled.</param>
        /// <returns>Returns the cropped image as a Brush.</returns>
        public static Brush Crop(int x, int y, int width, int height, bool isTiled = false)
        {
            if (spriteSheet == null)
            {
                throw new InvalidOperationException("The resource has to be initialized first.");
            }

            if (isTiled)
            {
                ImageBrush b = new ImageBrush(new CroppedBitmap(spriteSheet, new Int32Rect(x, y, width, height)));
                b.TileMode = TileMode.Tile;
                b.Viewport = new Rect(0, 0, 64, 64);
                b.ViewportUnits = BrushMappingMode.Absolute;
                return b;
            }

            return new ImageBrush(new CroppedBitmap(spriteSheet, new Int32Rect(x, y, width, height)));
        }
    }
}
