﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draw game related info.
    /// </summary>
    public class GameInfoDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameInfoDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public GameInfoDrawer(GameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                drawingContext.DrawRectangle(Brushes.Black, null, new Rect(0, 0, 1280, 50));

                FormattedText txtWave = new FormattedText($"Wave: {this.gameModel.EnemyWave}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("OCR A Extended"), 28, Brushes.White, 0d);
                FormattedText txtName = new FormattedText($"Player: {this.gameModel.Player.Name}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("OCR A Extended"), 28, Brushes.White, 0d);
                FormattedText txtScore = new FormattedText($"Score: {this.gameModel.Player.Score}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("OCR A Extended"), 28, Brushes.White, 0d);

                drawingContext.DrawText(txtWave, new Point(10, 10));
                drawingContext.DrawText(txtName, new Point(450, 10));
                drawingContext.DrawText(txtScore, new Point(1000, 10));
            }
        }
    }
}
