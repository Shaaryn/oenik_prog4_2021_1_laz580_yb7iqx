﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Class that draw healthbars.
    /// </summary>
    public static class HealthBarDrawer
    {
        /// <summary>
        /// Draws healthbar.
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        /// <param name="healthBar">IHealthbar instance.</param>
        /// <param name="maxHealth">Max health.</param>
        /// <param name="currentHealth">Current health.</param>
        public static void Draw(DrawingContext drawingContext, IHealthBar healthBar, double maxHealth, double currentHealth)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else if (healthBar == null)
            {
                throw new ArgumentNullException(nameof(healthBar), "HealthBar instance cannot be null!");
            }
            else if (currentHealth <= 0)
            {
                return;
            }
            else
            {
                double healthbarLenght = (currentHealth / maxHealth) * healthBar.Width;
                drawingContext.DrawRectangle(Brushes.Red, null, new Rect(healthBar.PositionX, healthBar.PositionY, healthbarLenght, healthBar.Height));
            }
        }
    }
}
