﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.Models.Weapons;
    using SlimeShooter.Models.States;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws weapons.
    /// </summary>
    public class WeaponDrawer : IDraw
    {
        private GameModel gameModel;
        private Brush currentBrush;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeaponDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public WeaponDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null.");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null.");
            }
            else
            {
                IWeapon weapon = this.gameModel.Player.CurrentWeapon;
                Rect weaponSize;

                if (weapon is Pistol)
                {
                    if (weapon.DirectionState == WeaponDirectionState.Left)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 12);
                        this.currentBrush = Assets.WeaponLeft;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.Right)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 12);
                        this.currentBrush = Assets.WeaponRight;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.Up)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 12, 20);
                        this.currentBrush = Assets.WeaponUp;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.Down)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 12, 20);
                        this.currentBrush = Assets.WeaponDown;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.RightDiagonalUp)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 16);
                        this.currentBrush = Assets.WeaponRightDiagonalUp;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.RightDiagonalDown)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 16);
                        this.currentBrush = Assets.WeaponRightDiagonalDown;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.LeftDiagonalUp)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 16);
                        this.currentBrush = Assets.WeaponLeftDiagonalUp;
                    }
                    else if (weapon.DirectionState == WeaponDirectionState.LeftDiagonalDown)
                    {
                        weaponSize = new Rect(this.gameModel.Player.CurrentWeapon.PositionX, this.gameModel.Player.CurrentWeapon.PositionY, 20, 16);
                        this.currentBrush = Assets.WeaponLeftDiagonalDown;
                    }

                    drawingContext.DrawRectangle(this.currentBrush, null, weaponSize);
                }
            }
        }
    }
}
