﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws a nexus crystal.
    /// </summary>
    public class NexusCrystalDrawer : IDraw
    {
        private GameModel gameModel;
        private int frameRate;
        private int nexusCrystalFrame;

        /// <summary>
        /// Initializes a new instance of the <see cref="NexusCrystalDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public NexusCrystalDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null!");
            }

            this.gameModel = gameModel;
            this.frameRate = 3;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                if (GameRenderer.Ticks % this.frameRate == 0)
                {
                    this.nexusCrystalFrame++;
                }

                if (this.nexusCrystalFrame > (Assets.Crystal.Length - 1))
                {
                    this.nexusCrystalFrame = 0;
                }

                drawingContext.DrawRectangle(Assets.Crystal[this.nexusCrystalFrame], null, new Rect(this.gameModel.Nexus.PositionX + 55, this.gameModel.Nexus.PositionY - 50, 30, 72));
            }
        }
    }
}
