﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws a nexus.
    /// </summary>
    public class NexusDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="NexusDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public NexusDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null!");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null.");
            }
            else
            {
                drawingContext.DrawRectangle(Assets.Nexus, null, new Rect(this.gameModel.Nexus.PositionX, this.gameModel.Nexus.PositionY, 140, 132));
                HealthBarDrawer.Draw(drawingContext, this.gameModel.Nexus.HealthBar, 500, this.gameModel.Nexus.HealtPoints);
            }
        }
    }
}
