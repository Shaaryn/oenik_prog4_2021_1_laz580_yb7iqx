﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.States;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class representing the animations of a player.
    /// </summary>
    public class PlayerDrawer : IDraw
    {
        private readonly int spriteWidth;
        private readonly int spriteHeight;
        private GameModel gameModel;
        private int frameRate;
        private PlayerMovementState previousState;
        private Brush currentBrush;
        private Brush previousBrush;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public PlayerDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel is null!");
            }

            this.gameModel = gameModel;
            this.frameRate = 3;
            this.spriteWidth = 48;
            this.spriteHeight = 63;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                if (this.previousBrush != null && this.gameModel.Player.MovementState == PlayerMovementState.Idle)
                {
                    drawingContext.DrawRectangle(this.previousBrush, null, new Rect(this.gameModel.Player.PositionX, this.gameModel.Player.PositionY, this.spriteWidth, this.spriteHeight));
                }
                else
                {
                    if ((GameRenderer.Ticks % this.frameRate) == 0)
                    {
                        this.gameModel.Player.CurrentSpriteIndex++;
                    }

                    if (this.gameModel.Player.CurrentSpriteIndex > 3 || this.previousState != this.gameModel.Player.MovementState)
                    {
                        this.gameModel.Player.CurrentSpriteIndex = 0;
                    }

                    if (this.gameModel.Player.MovementState == PlayerMovementState.MoveRight)
                    {
                        this.currentBrush = Assets.PlayerMoveRight[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveRight;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveLeft)
                    {
                        this.currentBrush = Assets.PlayerMoveLeft[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveLeft;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveUp)
                    {
                        this.currentBrush = Assets.PlayerMoveUp[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveUp;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveDown)
                    {
                        this.currentBrush = Assets.PlayerMoveDown[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveDown;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveRightDiagonalUp)
                    {
                        this.currentBrush = Assets.PlayerMoveRightDiagonalUp[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveRightDiagonalUp;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveRightDiagonalDown)
                    {
                        this.currentBrush = Assets.PlayerMoveRightDiagonalDown[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveRightDiagonalDown;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveLeftDiagonalUp)
                    {
                        this.currentBrush = Assets.PlayerMoveLeftDiagonalUp[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveLeftDiagonalUp;
                    }
                    else if (this.gameModel.Player.MovementState == PlayerMovementState.MoveLeftDiagonalDown)
                    {
                        this.currentBrush = Assets.PlayerMoveLeftDiagonalDown[this.gameModel.Player.CurrentSpriteIndex];
                        this.previousState = PlayerMovementState.MoveLeftDiagonalDown;
                    }

                    drawingContext.DrawRectangle(this.currentBrush, null, new Rect(this.gameModel.Player.PositionX, this.gameModel.Player.PositionY, this.spriteWidth, this.spriteHeight));
                    this.previousBrush = this.currentBrush;
                }
            }
        }
    }
}
