﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.States;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws slime monsters.
    /// </summary>
    public class SlimeMonsterDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="SlimeMonsterDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public SlimeMonsterDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null!");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                foreach (var slimeMonster in this.gameModel.SlimeMonsters)
                {
                    if (slimeMonster.CurrentState == SlimeMonsterState.MovingForward)
                    {
                        if (GameRenderer.Ticks % 3 == 0)
                        {
                            slimeMonster.CurrentFrameIndex++;
                        }

                        if (slimeMonster.CurrentFrameIndex > (Assets.SlimeMove.Length - 1))
                        {
                            slimeMonster.CurrentFrameIndex = 0;
                        }

                        drawingContext.DrawRectangle(Assets.SlimeMove[slimeMonster.CurrentFrameIndex], null, new Rect(slimeMonster.PositionX, slimeMonster.PositionY, 48, 48));
                        HealthBarDrawer.Draw(drawingContext, slimeMonster.HealthBar, slimeMonster.MaxHealtPoints, slimeMonster.HealtPoints);
                    }
                }
            }
        }
    }
}
