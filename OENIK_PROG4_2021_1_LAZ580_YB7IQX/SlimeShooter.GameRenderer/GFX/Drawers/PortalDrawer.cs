﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws portals.
    /// </summary>
    public class PortalDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="PortalDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public PortalDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel is null!");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                foreach (KeyValuePair<IPortal, bool> portal in this.gameModel.Portals)
                {
                    if (GameRenderer.Ticks % 4 == 0)
                    {
                        portal.Key.CurrentFrameIndex++;
                    }

                    if (portal.Key.CurrentFrameIndex > (Assets.RedPortal.Length - 1))
                    {
                        portal.Key.CurrentFrameIndex = 0;
                    }

                    drawingContext.DrawRectangle(Assets.RedPortal[portal.Key.CurrentFrameIndex], null, new Rect(portal.Key.PositionX, portal.Key.PositionY, 74, 126));
                }
            }
        }
    }
}
