﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Projectiles;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws projectiles.
    /// </summary>
    public class ProjectileDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectileDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public ProjectileDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null!");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                foreach (var projectile in this.gameModel.Player.TrackedProjectiles)
                {
                    if (projectile is PistolBullet)
                    {
                        drawingContext.DrawRectangle(Assets.PistolBullet, null, new Rect(projectile.PositionX, projectile.PositionY, 12, 12));
                    }
                }
            }
        }
    }
}
