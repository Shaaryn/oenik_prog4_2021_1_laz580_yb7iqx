﻿namespace SlimeShooter.Renderer.GFX
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Tiles;
    using SlimeShooter.Renderer.Interfaces;

    /// <summary>
    /// Class that draws the map.
    /// </summary>
    public class MapDrawer : IDraw
    {
        private GameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapDrawer"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public MapDrawer(GameModel gameModel)
        {
            if (gameModel == null)
            {
                throw new ArgumentNullException(nameof(gameModel), "GameModel cannot be null.");
            }

            this.gameModel = gameModel;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        public void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException(nameof(drawingContext), "Drawing context cannot be null!");
            }
            else
            {
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.gameModel.Map.GetLength(0); x++)
                {
                    for (int y = 0; y < this.gameModel.Map.GetLength(1); y++)
                    {
                        if (this.gameModel.Map[x, y] is GrassTile)
                        {
                            Geometry box = new RectangleGeometry(new Rect(y * 64, x * 64, 64, 64));
                            drawingContext.DrawGeometry(Assets.GrassTile, null, box);
                        }
                        else if (this.gameModel.Map[x, y] is DirtTile)
                        {
                            Geometry box = new RectangleGeometry(new Rect(y * 64, x * 64, 64, 64));
                            drawingContext.DrawGeometry(Assets.DirtTile, null, box);
                        }
                    }
                }
            }
        }
    }
}
