﻿namespace SlimeShooter.Renderer
{
    using System.Windows.Media;

    /// <summary>
    /// Holds every asset used in the game.
    /// </summary>
    public static class Assets
    {
        private const int WIDTH = 16;
        private const int HEIGHT = 21;

        /// <summary>
        /// Gets or sets the Grasstile brush.
        /// </summary>
        public static Brush GrassTile { get; set; }

        /// <summary>
        /// Gets or sets the DirtTile brush.
        /// </summary>
        public static Brush DirtTile { get; set; }

        /// <summary>
        /// Gets or sets the Player brush.
        /// </summary>
        public static Brush Player { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveRight brush.
        /// </summary>
        public static Brush[] PlayerMoveRight { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveLeft brush.
        /// </summary>
        public static Brush[] PlayerMoveLeft { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveUp brush.
        /// </summary>
        public static Brush[] PlayerMoveUp { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveDown brush.
        /// </summary>
        public static Brush[] PlayerMoveDown { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveRightDiagonalUp brush.
        /// </summary>
        public static Brush[] PlayerMoveRightDiagonalUp { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveRightDiagonalDown brush.
        /// </summary>
        public static Brush[] PlayerMoveRightDiagonalDown { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveLeftDiagonalUp brush.
        /// </summary>
        public static Brush[] PlayerMoveLeftDiagonalUp { get; set; }

        /// <summary>
        /// Gets or sets the PlayerMoveLeftDiagonalDown brush.
        /// </summary>
        public static Brush[] PlayerMoveLeftDiagonalDown { get; set; }

        /// <summary>
        /// Gets or sets the WeaponRight brush.
        /// </summary>
        public static Brush WeaponRight { get; set; }

        /// <summary>
        /// Gets or sets the WeaponLeft brush.
        /// </summary>
        public static Brush WeaponLeft { get; set; }

        /// <summary>
        /// Gets or sets the WeaponUp brush.
        /// </summary>
        public static Brush WeaponUp { get; set; }

        /// <summary>
        /// Gets or sets the WeaponDown brush.
        /// </summary>
        public static Brush WeaponDown { get; set; }

        /// <summary>
        /// Gets or sets the WeaponRightDiagonalUp brush.
        /// </summary>
        public static Brush WeaponRightDiagonalUp { get; set; }

        /// <summary>
        /// Gets or sets the WeaponRightDiagonalDown brush.
        /// </summary>
        public static Brush WeaponRightDiagonalDown { get; set; }

        /// <summary>
        /// Gets or sets the WeaponLeftDiagonalUp brush.
        /// </summary>
        public static Brush WeaponLeftDiagonalUp { get; set; }

        /// <summary>
        /// Gets or sets the WeaponLeftDiagonalDown brush.
        /// </summary>
        public static Brush WeaponLeftDiagonalDown { get; set; }

        /// <summary>
        /// Gets or sets the PistolBullet brush.
        /// </summary>
        public static Brush PistolBullet { get; set; }

        /// <summary>
        /// Gets or sets the Nexus brush.
        /// </summary>
        public static Brush Nexus { get; set; }

        /// <summary>
        /// Gets or sets the Crystal brush.
        /// </summary>
        public static Brush[] Crystal { get; set; }

        /// <summary>
        /// Gets or sets the SlimeMove brush.
        /// </summary>
        public static Brush[] SlimeMove { get; set; }

        /// <summary>
        /// Gets or sets the SlimeAttack brush.
        /// </summary>
        public static Brush[] SlimeAttack { get; set; }

        /// <summary>
        /// Gets or sets the RedPortal brush.
        /// </summary>
        public static Brush[] RedPortal { get; set; }

        /// <summary>
        /// Initialites the assets.
        /// </summary>
        public static void Init()
        {
            SpriteSheetHandler.SetResource(@".resources\Images\Slime_shooter_sprite_sheet.png");
            Player = SpriteSheetHandler.Crop(0, 0, WIDTH, HEIGHT);
            GrassTile = SpriteSheetHandler.Crop(288, 0, 16, 16, true);
            DirtTile = SpriteSheetHandler.Crop(288, 32, 16, 16, true);

            PlayerMoveLeft = new Brush[4];

            PlayerMoveRight = new Brush[4];
            PlayerMoveRight[0] = SpriteSheetHandler.Crop(32, 0, WIDTH, HEIGHT);
            PlayerMoveRight[1] = SpriteSheetHandler.Crop(64, 0, WIDTH, HEIGHT);
            PlayerMoveRight[2] = SpriteSheetHandler.Crop(96, 0, WIDTH, HEIGHT);
            PlayerMoveRight[3] = SpriteSheetHandler.Crop(128, 0, WIDTH, HEIGHT);

            PlayerMoveLeft = new Brush[4];
            PlayerMoveLeft[0] = SpriteSheetHandler.Crop(32, 32, WIDTH, HEIGHT);
            PlayerMoveLeft[1] = SpriteSheetHandler.Crop(64, 32, WIDTH, HEIGHT);
            PlayerMoveLeft[2] = SpriteSheetHandler.Crop(96, 32, WIDTH, HEIGHT);
            PlayerMoveLeft[3] = SpriteSheetHandler.Crop(128, 32, WIDTH, HEIGHT);

            PlayerMoveUp = new Brush[4];
            PlayerMoveUp[0] = SpriteSheetHandler.Crop(160, 0, WIDTH, HEIGHT);
            PlayerMoveUp[1] = SpriteSheetHandler.Crop(192, 0, WIDTH, HEIGHT);
            PlayerMoveUp[2] = SpriteSheetHandler.Crop(224, 0, WIDTH, HEIGHT);
            PlayerMoveUp[3] = SpriteSheetHandler.Crop(256, 0, WIDTH, HEIGHT);

            PlayerMoveDown = new Brush[4];
            PlayerMoveDown[0] = SpriteSheetHandler.Crop(160, 32, WIDTH, HEIGHT);
            PlayerMoveDown[1] = SpriteSheetHandler.Crop(192, 32, WIDTH, HEIGHT);
            PlayerMoveDown[2] = SpriteSheetHandler.Crop(224, 32, WIDTH, HEIGHT);
            PlayerMoveDown[3] = SpriteSheetHandler.Crop(256, 32, WIDTH, HEIGHT);

            PlayerMoveRightDiagonalUp = new Brush[4];
            PlayerMoveRightDiagonalUp[0] = SpriteSheetHandler.Crop(160, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalUp[1] = SpriteSheetHandler.Crop(192, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalUp[2] = SpriteSheetHandler.Crop(224, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalUp[3] = SpriteSheetHandler.Crop(256, 64, WIDTH, HEIGHT);

            PlayerMoveRightDiagonalDown = new Brush[4];
            PlayerMoveRightDiagonalDown[0] = SpriteSheetHandler.Crop(32, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalDown[1] = SpriteSheetHandler.Crop(64, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalDown[2] = SpriteSheetHandler.Crop(96, 64, WIDTH, HEIGHT);
            PlayerMoveRightDiagonalDown[3] = SpriteSheetHandler.Crop(128, 64, WIDTH, HEIGHT);

            PlayerMoveLeftDiagonalUp = new Brush[4];
            PlayerMoveLeftDiagonalUp[0] = SpriteSheetHandler.Crop(160, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalUp[1] = SpriteSheetHandler.Crop(192, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalUp[2] = SpriteSheetHandler.Crop(224, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalUp[3] = SpriteSheetHandler.Crop(256, 96, WIDTH, HEIGHT);

            PlayerMoveLeftDiagonalDown = new Brush[4];
            PlayerMoveLeftDiagonalDown[0] = SpriteSheetHandler.Crop(32, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalDown[1] = SpriteSheetHandler.Crop(64, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalDown[2] = SpriteSheetHandler.Crop(96, 96, WIDTH, HEIGHT);
            PlayerMoveLeftDiagonalDown[3] = SpriteSheetHandler.Crop(128, 96, WIDTH, HEIGHT);

            WeaponRight = SpriteSheetHandler.Crop(96, 128, 10, 6);
            WeaponLeft = SpriteSheetHandler.Crop(96, 160, 10, 6);
            WeaponUp = SpriteSheetHandler.Crop(128, 128, 6, 10);
            WeaponDown = SpriteSheetHandler.Crop(64, 128, 6, 10);
            WeaponRightDiagonalUp = SpriteSheetHandler.Crop(32, 128, 10, 8);
            WeaponRightDiagonalDown = SpriteSheetHandler.Crop(0, 128, 10, 8);
            WeaponLeftDiagonalUp = SpriteSheetHandler.Crop(32, 160, 10, 8);
            WeaponLeftDiagonalDown = SpriteSheetHandler.Crop(0, 160, 10, 8);

            PistolBullet = SpriteSheetHandler.Crop(160, 128, 6, 6);

            SlimeMove = new Brush[4];
            SlimeMove[0] = SpriteSheetHandler.Crop(128, 320, 16, 14);
            SlimeMove[1] = SpriteSheetHandler.Crop(160, 320, 16, 15);
            SlimeMove[2] = SpriteSheetHandler.Crop(192, 320, 16, 16);
            SlimeMove[3] = SpriteSheetHandler.Crop(224, 320, 16, 15);

            SlimeAttack = new Brush[8];
            SlimeAttack[0] = SpriteSheetHandler.Crop(0, 288, 16, 15);
            SlimeAttack[1] = SpriteSheetHandler.Crop(32, 288, 16, 17);
            SlimeAttack[2] = SpriteSheetHandler.Crop(64, 288, 18, 20);
            SlimeAttack[3] = SpriteSheetHandler.Crop(96, 288, 20, 23);
            SlimeAttack[4] = SpriteSheetHandler.Crop(128, 288, 24, 27);
            SlimeAttack[5] = SpriteSheetHandler.Crop(160, 288, 28, 29);
            SlimeAttack[6] = SpriteSheetHandler.Crop(192, 288, 28, 29);
            SlimeAttack[7] = SpriteSheetHandler.Crop(224, 288, 28, 29);

            Nexus = SpriteSheetHandler.Crop(0, 192, 46, 44);

            Crystal = new Brush[4];
            Crystal[0] = SpriteSheetHandler.Crop(64, 192, 10, 24);
            Crystal[1] = SpriteSheetHandler.Crop(96, 192, 10, 24);
            Crystal[2] = SpriteSheetHandler.Crop(128, 192, 10, 24);
            Crystal[3] = SpriteSheetHandler.Crop(160, 192, 10, 24);

            RedPortal = new Brush[4];
            RedPortal[0] = SpriteSheetHandler.Crop(0, 384, 26, 42);
            RedPortal[1] = SpriteSheetHandler.Crop(32, 384, 26, 42);
            RedPortal[2] = SpriteSheetHandler.Crop(64, 384, 26, 42);
            RedPortal[3] = SpriteSheetHandler.Crop(96, 384, 26, 42);
        }
    }
}
