﻿namespace SlimeShooter.Renderer.Interfaces
{
    using System.Windows.Media;

    /// <summary>
    /// Interface for objects which should draw to the drawing context.
    /// </summary>
    public interface IDraw
    {
        /// <summary>
        /// Draws to the drawing context.
        /// </summary>
        /// <param name="drawingContext">DrawingContext instance.</param>
        void Draw(DrawingContext drawingContext);
    }
}
