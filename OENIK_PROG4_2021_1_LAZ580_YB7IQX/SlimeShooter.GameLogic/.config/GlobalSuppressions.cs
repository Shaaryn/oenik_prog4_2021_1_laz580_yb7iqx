﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GlobalSuppressions.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Im used to have ctor after fields and properties. I think its more readable.")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "Overusage result in eye cancer in my opinion and reduces readability.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "The member we inherit from already contains")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Contains frequently changing properties.")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "It is not for encryption purposes.")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1119:Statement should not use unnecessary parenthesis", Justification = "Helps me maintain readability.")]
[assembly: SuppressMessage("Maintainability", "CA1508:Avoid dead conditional code", Justification = "It is not a dead condotion.")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Easier to handle it this way.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Would be nice to have tho...")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1110:Opening parenthesis or bracket should be on declaration line", Justification = "Got really long and complex, so its probably better this way.")]