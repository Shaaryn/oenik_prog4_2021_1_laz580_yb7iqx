﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MainMenuSceneLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.MainMenuScene
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using AutoMapper;
    using SlimeShooter.GameLogic.MainMenuScene.Helper;
    using SlimeShooter.Logic.MainMenuScene.Interfaces;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;
    using SlimeShooter.Models.Tiles;
    using SlimeShooter.Repositories;

    /// <summary>
    /// Logic class responsible for handling and managing the main menu scene as a whole.
    /// </summary>
    public class MainMenuSceneLogic : IMainMenuSceneLogic
    {
        private PlayerRepository playerRepo;
        private IMapper mapper;

        /// <summary>
        /// DI GameModel.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuSceneLogic"/> class.
        /// </summary>
        /// <param name="model">Model of the game.</param>
        /// <param name="playerRepo">Repository that contains the Players of the leaderboard.</param>
        public MainMenuSceneLogic(GameModel model, PlayerRepository playerRepo)
        {
            this.Model = model;
            this.playerRepo = playerRepo;
            this.mapper = MapperFactory.CreateMapper();
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public void LoadGame()
        {
            // string path = Directory.GetCurrentDirectory() + $"\\Saves\\save.sav";
            string path = @"..\..\..\..\SlimeShooter.GameLogic\.resources\Saves\save.sav";
            XDocument xdoc = XDocument.Load(path);
            XElement root = xdoc.Root;

            // Player
            XElement playerNode = root.FirstNode as XElement;
            this.Model.Player.Id = Convert.ToInt32(playerNode.FirstAttribute.Value);
            this.Model.Player.Name = playerNode.FirstAttribute.NextAttribute.Value;

            IEnumerable<XNode> subPlayerNodes = playerNode.Nodes();
            foreach (XNode item in subPlayerNodes)
            {
                XElement tmpNode = item as XElement;

                if (tmpNode.Name == nameof(this.Model.Player.Score))
                {
                    float value;
                    float.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.Player.Score = value;
                }
                else if (tmpNode.Name == nameof(this.Model.Player.PositionX))
                {
                    double value;
                    double.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.Player.PositionX = value;
                }
                else if (tmpNode.Name == nameof(this.Model.Player.PositionY))
                {
                    double value;
                    double.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.Player.PositionY = value;
                }
            }

            // Enemy
            XElement enemyNode = playerNode.NextNode as XElement;

            IEnumerable<XNode> subEnemyNodes = enemyNode.Nodes();
            foreach (XNode item in subEnemyNodes)
            {
                XElement tmpNode = item as XElement;

                if (tmpNode.Name == nameof(this.Model.EnemyWave))
                {
                    int value;
                    int.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.EnemyWave = value;
                }
                else if (tmpNode.Name == nameof(this.Model.EnemyPowerUp))
                {
                    double value;
                    double.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.EnemyPowerUp = value;
                }
                else if (tmpNode.Name == nameof(this.Model.EnemyNumber))
                {
                    int value;
                    int.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out value);
                    this.Model.EnemyNumber = value;
                }
            }

            // GameSettings
            XElement settingsNode = enemyNode.NextNode as XElement;

            IEnumerable<XNode> subSettingsNodes = settingsNode.Nodes();
            foreach (XNode item in subSettingsNodes)
            {
                XElement tmpNode = item as XElement;

                if (tmpNode.Name == nameof(this.Model.GameSettings.MapType))
                {
                    if (Enum.TryParse(tmpNode.Value, out MapType mapType))
                    {
                        this.Model.GameSettings.MapType = mapType;
                    }
                }
                else if (tmpNode.Name == nameof(this.Model.GameSettings.Difficulty))
                {
                    if (Enum.TryParse(tmpNode.Value, out Difficulty difficulty))
                    {
                        this.Model.GameSettings.Difficulty = difficulty;
                    }
                }
                else if (tmpNode.Name == nameof(this.Model.Map))
                {
                    int lengthX;
                    int lengthY;
                    int.TryParse(tmpNode.FirstAttribute.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out lengthX);
                    int.TryParse(tmpNode.FirstAttribute.NextAttribute.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out lengthY);
                    this.Model.Map = new ITile[lengthX, lengthY];

                    IEnumerable<XNode> subMapNodes = tmpNode.Nodes();
                    foreach (XNode row in subMapNodes)
                    {
                        XElement tmpRow = row as XElement;

                        int currentX;
                        int.TryParse(tmpRow.FirstAttribute.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out currentX);
                        for (int j = 0; j < lengthY; j++)
                        {
                            if (tmpRow.Value[j].ToString() == "o")
                            {
                                this.Model.Map[currentX, j] = new GrassTile(0, 0);
                            }
                            else if (tmpRow.Value[j].ToString() == "x")
                            {
                                this.Model.Map[currentX, j] = new DirtTile(0, 0);
                            }
                        }
                    }
                }
            }

            // Nexus
            XElement nexusNode = settingsNode.NextNode as XElement;

            int positionX = default;
            int positionY = default;
            double healthPoints = default;

            IEnumerable<XNode> subNexusNodes = nexusNode.Nodes();
            foreach (XNode item in subNexusNodes)
            {
                XElement tmpNode = item as XElement;

                if (tmpNode.Name == nameof(this.Model.Nexus.PositionX))
                {
                    int.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out positionX);
                }
                else if (tmpNode.Name == nameof(this.Model.Nexus.PositionY))
                {
                    int.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out positionY);
                }
                else if (tmpNode.Name == nameof(this.Model.Nexus.HealtPoints))
                {
                    double.TryParse(tmpNode.Value, NumberStyles.AllowDecimalPoint, new CultureInfo("en-US"), out healthPoints);
                }
            }

            this.Model.Nexus = new Nexus(positionX, positionY, healthPoints);
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <returns>List of players that are stored in the database.</returns>
        public List<Models.Player> FetchLeaderboard()
        {
            IQueryable<Data.Entities.Player> players = playerRepo.GetAll();
            List<Models.Player> leaderboard = new List<Models.Player>();
            foreach (Data.Entities.Player player in players)
            {
                leaderboard.Add(mapper.Map<Data.Entities.Player, Models.Player>(player));
            }

            return leaderboard.OrderByDescending(player => player.Score).ToList();
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        public void AddNewPlayer(string playerName)
        {
            this.Model.Player.Name = playerName;
            this.playerRepo.Insert(this.mapper.Map<Models.Player, Data.Entities.Player>(this.Model.Player));

            this.Model.Player.Id = this.playerRepo.GetAll().OrderBy(player => player.Id).Last().Id;
        }
    }
}
