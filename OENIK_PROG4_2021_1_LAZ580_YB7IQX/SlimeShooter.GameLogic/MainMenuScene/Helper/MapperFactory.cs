﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MapperFactory.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.MainMenuScene.Helper
{
    using AutoMapper;

    /// <summary>
    /// Mapper factory defining the transition between classes.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Mapping route for the properties.
        /// </summary>
        /// <returns>IMapper that contains the routing information.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SlimeShooter.Data.Entities.Player, SlimeShooter.Models.Player>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                ForMember(dest => dest.Score, map => map.MapFrom(src => src.Score)).ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}
