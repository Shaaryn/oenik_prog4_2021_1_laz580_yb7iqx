﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IMainMenuSceneLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.MainMenuScene.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface defininf the necessary functions and/or properties for the MainMenuLogic class.
    /// Responsible for the flow control of the whole main menu scene.
    /// </summary>
    internal interface IMainMenuSceneLogic
    {
        /// <summary>
        /// Responsible for loading a .save save file from the defined directory.
        /// </summary>
        public void LoadGame();

        /// <summary>
        /// Responsible for fetching the database that contains the Players of the leaderboard.
        /// </summary>
        /// <returns>List of players that are stored in the database.</returns>
        public List<Models.Player> FetchLeaderboard();

        /// <summary>
        /// Defines a new name for the player.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        public void AddNewPlayer(string playerName);
    }
}
