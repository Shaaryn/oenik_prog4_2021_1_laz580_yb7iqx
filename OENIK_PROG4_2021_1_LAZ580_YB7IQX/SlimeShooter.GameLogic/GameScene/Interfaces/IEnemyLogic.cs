﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IEnemyLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.Interfaces.GameScene.Interfaces
{
    using System.Collections.ObjectModel;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Interface defining the necessary functions and/or properties for the EnemyLogic class.
    /// Responsible for every Enemy related logic.
    /// </summary>
    public interface IEnemyLogic
    {
        /// <summary>
        /// Generates x number of new Enemies to fight against the player.
        /// </summary>
        void GenerateWave();

        /// <summary>
        /// Moves all of the enemies towards the nexus.
        /// </summary>
        void Move();

        /// <summary>
        /// Deals damage (blows up) to the Player's nexus.
        /// </summary>
        /// <param name="nexus">Player's <see cref="INexus">nexus</see> that we deal damage to.</param>
        /// <param name="enemy">Enemy character that deals the damage(blows up).</param>
        void DealDamage(INexus nexus, IEnemy enemy);

        /// <summary>
        /// Deals damage to the enemy based on the projectile it gets hit by.
        /// </summary>
        /// <param name="enemy">Enemy character that suffers the damage.</param>
        /// <param name="projectile">Projectile that deals the damage to the enemy character.</param>
        void SufferDamage(IEnemy enemy, IProjectile projectile);
    }
}
