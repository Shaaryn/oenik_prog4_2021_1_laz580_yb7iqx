﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IGameSceneLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.Interfaces.GameScene.Interfaces
{
    using SlimeShooter.Models;

    /// <summary>
    /// Interface defining the necessary functions and/or properties for the GameSceneLogic class.
    /// Responsible for the flow control of the whole game scene.
    /// </summary>
    internal interface IGameSceneLogic
    {
        /// <summary>
        /// Model representing all data on the screen.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// Gets the logic responsible for the Player actions.
        /// DI.
        /// </summary>
        public IPlayerLogic PlayerLogic { get; }

        /// <summary>
        /// Gets the logic responsible for the Enemy actions.
        /// </summary>
        public IEnemyLogic EnemyLogic { get; }

        /// <summary>
        /// Function that executes the other logic's functions (which needs to be executed in every tick) in order.
        /// </summary>
        /// <param name="cursorX">Position of the cursor on the X axis.</param>
        /// <param name="cursorY">Position of the cursor on the Y axis.</param>
        public void ExecuteTick(double cursorX, double cursorY);

        /// <summary>
        /// Saves the current player's score if it is higher than the score stored in the database.
        /// </summary>
        public void SaveScore();
    }
}
