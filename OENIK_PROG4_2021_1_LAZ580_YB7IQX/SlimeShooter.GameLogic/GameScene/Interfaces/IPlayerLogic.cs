﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="IPlayerLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.Interfaces.GameScene.Interfaces
{
    using System.Collections.ObjectModel;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.Models.Interfaces;

    /// <summary>
    /// Interface defining the necessary functions and/or properties for the PlayerLogic class.
    /// Responsible for every Player related logic.
    /// </summary>
    public interface IPlayerLogic
    {
        /// <summary>
        /// Moves the character towards the cursor's position.
        /// </summary>
        /// <param name="direction">Direction in which the player wants to move.</param>
        void Move(MovementDirection direction);

        /// <summary>
        /// Rotates the character and its currently held weapon to face towards the cursor's position.
        /// </summary>
        /// <param name="cursorX">Current position of the cursor on the X plane.</param>
        /// <param name="cursorY">Current position of the cursor on the Y plane.</param>
        void Look(double cursorX, double cursorY);

        /// <summary>
        /// Moves the tracked projectiles according their stored step values.
        /// </summary>
        void MoveProjectiles();

        /// <summary>
        /// Fires the currently selected weapon.
        /// </summary>
        void Shoot();

        /// <summary>
        /// Swaps the currently selected wapon with another wapon.
        /// </summary>
        /// <param name="newIndex">New index of the selected weapon.</param>
        void SwapWeapon(int newIndex);
    }
}
