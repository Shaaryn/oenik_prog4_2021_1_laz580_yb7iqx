﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="EnemyLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.GameScene
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using SlimeShooter.Logic.Interfaces.GameScene.Interfaces;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Logic class responsible for handling anything that can occur to the enemy (or enemies).
    /// </summary>
    public sealed class EnemyLogic : IEnemyLogic
    {
        private static readonly Random Rnd = new Random();

        /// <summary>
        /// Represents the used padding of the spawning area.
        /// </summary>
        private static int enemySpawnArePadding = 50;

        /// <summary>
        /// Represent the lower bounds of each spawn are on each axis(X,Y).
        /// </summary>
        private static int[][] enemySpawnAreaLB = new int[][]
        {
            // Left side
            new int[] { 0, 180 },

            // Top side
            new int[] { 0, 0 },

            // Right side
            new int[] { 960, 180 },

            // Bottom side
            new int[] { 0, 540 },
        };

        /// <summary>
        /// Represent the upper bounds of each spawn are on each axis(X,Y).
        /// </summary>
        private static int[][] enemySpawnAreaUB = new int[][]
        {
            // Left side
            new int[] { 320, 540 },

            // Top side
            new int[] { 1280, 180 },

            // Right side
            new int[] { 1280, 540 },

            // Bottom side
            new int[] { 1280, 720 },
        };

        // private Dictionary<int, bool> usablePortals = new Dictionary<int, bool>();

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyLogic"/> class.
        /// </summary>
        /// <param name="model">Model of the game.</param>
        public EnemyLogic(GameModel model)
        {
            this.Model = model;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public void Move()
        {
            for (int i = 0; i < this.Model.SlimeMonsters.Count; i++)
            {
                Model.SlimeMonsters[i].PositionX += Model.SlimeMonsters[i].StepX;
                Model.SlimeMonsters[i].PositionY += Model.SlimeMonsters[i].StepY;

                // Hitbox displacement :
                // CS1612 Cannot modify the return value of the hitbox because it is not a variable
                Size tmpSize = new Size(this.Model.SlimeMonsters[i].HitBox.Width, this.Model.SlimeMonsters[i].HitBox.Height);
                Point newLocation = new Point(this.Model.SlimeMonsters[i].PositionX, this.Model.SlimeMonsters[i].PositionY);
                Rect newHitBox = new Rect(newLocation, tmpSize);
                this.Model.SlimeMonsters[i].HitBox = newHitBox;
            }

            return;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <param name="nexus">Player's <see cref="INexus">nexus</see> that we deal damage to.</param>
        /// <param name="enemy">Enemy character that deals the damage(blows up).</param>
        public void DealDamage(INexus nexus, IEnemy enemy)
        {
            // Should blow up too.
            if (nexus != null && enemy != null)
            {
                nexus.HealtPoints -= enemy.Damage;

                // enemy.CurrentState = SlimeMonsterState.Explode;
                // this.trackedEnemies.Remove(enemy);
            }

            return;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <param name="enemy">Enemy character that suffers the damage.</param>
        /// <param name="projectile">Projectile that deals the damage to the enemy character.</param>
        public void SufferDamage(IEnemy enemy, IProjectile projectile)
        {
            if (enemy != null && projectile != null)
            {
                int enemyIndex = this.Model.SlimeMonsters.IndexOf(enemy);
                Model.SlimeMonsters[enemyIndex].HealtPoints -= projectile.Damage;

                if (Model.SlimeMonsters[enemyIndex].HealtPoints <= 0)
                {
                    // trackedEnemies[enemyIndex].CurrentState = SlimeMonsterState.Explode;
                    Model.SlimeMonsters[enemyIndex].StepX = 0;
                    Model.SlimeMonsters[enemyIndex].StepY = 0;
                    Model.Player.Score += (float)Model.SlimeMonsters[enemyIndex].MaxHealtPoints;
                }
            }

            return;
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public void GenerateWave()
        {
            Model.EnemyWave++;
            Model.MillisecondsTillNextWave -= (int)Math.Ceiling(Math.Log2((double)Model.EnemyWave) * 1000);
            if (Model.EllapsedTime.IsRunning)
            {
                Model.EllapsedTime.Restart();
            }
            else
            {
                Model.EllapsedTime.Start();
            }

            switch (this.Model.GameSettings.MapType)
            {
                case MapType.Random:
                    break;
                case MapType.File:

                    // Every 10th wave is (wave/10) times more difficult. !FOR THIS WAVE ONLY!
                    if (Model.EnemyWave % 10 == 0)
                    {
                        int times = this.Model.EnemyWave / 10;
                        this.Model.EnemyNumber += times;

                        for (int i = 0; i < this.Model.EnemyNumber; i++)
                        {
                            RandomSpawn();
                        }

                        this.Model.EnemyNumber -= times;
                    }
                    else
                    {
                        int lowerB = 0;
                        int upperB = 1;
                        double pUp = 0;
                        switch (this.Model.GameSettings.Difficulty)
                        {
                            case Difficulty.Easy:
                                lowerB = 4;
                                upperB = 6;
                                pUp = 0.25;
                                break;
                            case Difficulty.Hard:
                                lowerB = 3;
                                upperB = 5;
                                pUp = 0.35;
                                break;
                        }

                        // Every 3 or 4 waves we increment the number of spawning enemies by one.
                        if (Model.EnemyWave % Rnd.Next(lowerB, upperB) == 0)
                        {
                            this.Model.EnemyNumber++;
                        }

                        // Every 3 or 4 waves we increas the strength of the enmies by small amount.
                        if (Model.EnemyWave % Rnd.Next(lowerB, upperB) == 0)
                        {
                            this.Model.EnemyPowerUp += pUp;
                        }

                        for (int i = 0; i < this.Model.EnemyNumber; i++)
                        {
                            RandomSpawn();
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Decides a random spawn point for the enemy.
        /// </summary>
        /// <returns>X and Y coordinates of the enemy's spawn point.</returns>
        private static (double, double) CalculateRandomPosition()
        {
            // 0 - Right side, 1 - Top side, 2 - Right side, 3 - Bottom side
            int rndSide = Rnd.Next(0, 4);

            int rndCoordX = 0;
            int rndCoordY = 0;

            switch (rndSide)
            {
                // Left side
                case 0:
                    rndCoordX = Rnd.Next((enemySpawnAreaLB[0][0] + enemySpawnArePadding), (enemySpawnAreaUB[0][0] - enemySpawnArePadding));
                    rndCoordY = Rnd.Next((enemySpawnAreaLB[0][1] + enemySpawnArePadding), (enemySpawnAreaUB[0][1] - enemySpawnArePadding));
                    break;

                // Top side
                case 1:
                    rndCoordX = Rnd.Next((enemySpawnAreaLB[1][0] + enemySpawnArePadding), (enemySpawnAreaUB[1][0] - enemySpawnArePadding));
                    rndCoordY = Rnd.Next((enemySpawnAreaLB[1][1] + enemySpawnArePadding), (enemySpawnAreaUB[1][1] - enemySpawnArePadding));
                    break;

                // Right side
                case 2:
                    rndCoordX = Rnd.Next((enemySpawnAreaLB[2][0] + enemySpawnArePadding), (enemySpawnAreaUB[2][0] - enemySpawnArePadding));
                    rndCoordY = Rnd.Next((enemySpawnAreaLB[2][1] + enemySpawnArePadding), (enemySpawnAreaUB[2][1] - enemySpawnArePadding));
                    break;

                // Bottom side
                case 3:
                    rndCoordX = Rnd.Next((enemySpawnAreaLB[3][0] + enemySpawnArePadding), (enemySpawnAreaUB[3][0] - enemySpawnArePadding));
                    rndCoordY = Rnd.Next((enemySpawnAreaLB[3][1] + enemySpawnArePadding), (enemySpawnAreaUB[3][1] - enemySpawnArePadding));
                    break;
            }

            return ((double)rndCoordX, (double)rndCoordY);
        }

        /// <summary>
        /// Spawns down an enemy at a random position, near around the edge of the window.
        /// </summary>
        private void RandomSpawn()
        {
            double spawnPositionX;
            double spawnPositionY;

            (spawnPositionX, spawnPositionY) = CalculateRandomPosition();

            IEnemy enemy = new SlimeMonster(spawnPositionX, spawnPositionY, Model.EnemyPowerUp);
            enemy.CurrentState = SlimeMonsterState.MovingForward;
            CalculateVector(enemy);
            this.Model.SlimeMonsters.Add(enemy);
        }

        /// <summary>
        /// Calculates the constant movement vector for the passed enemy relative to the nexus' position.
        /// </summary>
        /// <param name="enemy"><see cref="IEnemy">IEnemy</see> object for which we want to calculate the step for.</param>
        private void CalculateVector(IEnemy enemy)
        {
            // Displacement on the X and Y plane relative to the player.
            double distanceX = this.Model.Nexus.PositionX - enemy.PositionX;
            double distanceY = this.Model.Nexus.PositionY - enemy.PositionY;
            double angle = Math.Atan2(distanceY, distanceX);    // Note: Angle is in rad.

            // Cartesian coordinates from the above polar variables.
            enemy.StepX = enemy.Speed * Math.Cos(angle);
            enemy.StepY = enemy.Speed * Math.Sin(angle);

            return;
        }
    }
}
