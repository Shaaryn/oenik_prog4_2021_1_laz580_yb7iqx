﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PlayerLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.GameScene
{
    using System;
    using System.Windows;
    using SlimeShooter.GameLogic.GameScene;
    using SlimeShooter.GameLogic.GameScene.Helper;
    using SlimeShooter.Logic.Interfaces.GameScene.Interfaces;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Interfaces;
    using SlimeShooter.Models.Interfaces.Categories;
    using SlimeShooter.Models.Models.Weapons;
    using SlimeShooter.Models.Projectiles;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Logic class responsible for handling anything that can occur to the player.
    /// </summary>
    public sealed class PlayerLogic : IPlayerLogic
    {
        private static Random rnd = new Random();

        /// <summary>
        /// Defines the offset values for each possible direction the weapon may point towards to.
        /// </summary>
        private static double[][] weaponOffset = new double[][]
        {
            new double[] { -5, 35 },        // 0 (left-up)
            new double[] { 20, 20 },        // 1 (up)
            new double[] { 27.5, 30 },      // 2 (right-up)
            new double[] { 30, 37.5 },      // 3 (right)
            new double[] { 25, 40 },        // 4 (right-down)
            new double[] { 17.5, 50 },      // 5 (down)
            new double[] { -5, 40 },        // 6 (left-down)
            new double[] { -5, 35 },        // 7 (down)
        };

        private ModelCache cash;
        private double cursorX;
        private double cursorY;

        /// <summary>
        /// Model of the game.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="model">Model containing the game data.</param>
        /// <param name="cash">Model containing cash information.</param>
        public PlayerLogic(GameModel model, ModelCache cash)
        {
            this.cash = cash;
            this.Model = model;

            this.Model.Player.CurrentWeapon = new Pistol(
                "pistol",
                this.Model.Player.PositionX + weaponOffset[0][0],
                this.Model.Player.PositionY + weaponOffset[0][1],
                0,
                new Rect(50, 50, 50, 50),
                new PistolBullet(50, 50, 10, 25));
        }

        // Tick functions

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="cursorX">Current position of the cursor on the X plane.</param>
        /// <param name="cursorY">Current position of the cursor on the Y plane.</param>
        public void Look(double cursorX, double cursorY)
        {
            // Cash
            if (this.Model.Player.MovementState != PlayerMovementState.Idle)
            {
                this.cash.MovementState = this.Model.Player.MovementState;
            }

            this.cash.PlayerPositionX = this.Model.Player.PositionX;
            this.cash.PlayerPositionY = this.Model.Player.PositionY;

            this.cursorX = cursorX;
            this.cursorY = cursorY;

            double distanceX = this.cursorX - this.Model.Player.PositionX;
            double distanceY = this.cursorY - this.Model.Player.PositionY;
            double angle = Math.Atan2(distanceY, distanceX);
            double degree = angle * 180 / Math.PI;

            DecidePlayerDirection(degree);
            DecideWeaponDirection(degree);
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public void MoveProjectiles()
        {
            for (int i = 0; i < this.Model.Player.TrackedProjectiles.Count; i++)
            {
                bool isOutOfWindow = this.OutOfWindow(this.Model.Player.TrackedProjectiles[i]);
                if (!isOutOfWindow)
                {
                    this.Model.Player.TrackedProjectiles[i].PositionX += this.Model.Player.TrackedProjectiles[i].StepX;
                    this.Model.Player.TrackedProjectiles[i].PositionY += this.Model.Player.TrackedProjectiles[i].StepY;

                    // Hitbox displacement :
                    // CS1612 Cannot modify the return value of the hitbox because it is not a variable
                    Size tmpSize = new Size(this.Model.Player.TrackedProjectiles[i].HitBox.Width, this.Model.Player.TrackedProjectiles[i].HitBox.Height);
                    Point newLocation = new Point(this.Model.Player.TrackedProjectiles[i].PositionX, this.Model.Player.TrackedProjectiles[i].PositionY);
                    Rect newHitBox = new Rect(newLocation, tmpSize);
                    this.Model.Player.TrackedProjectiles[i].HitBox = newHitBox;
                }
            }

            return;
        }

        // Control functions

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="direction">Direction in which the player wants to move.</param>
        public void Move(MovementDirection direction)
        {
            // Cash
            this.cash.IdleCount = 0;
            this.Model.Player.MovementState = this.cash.MovementState;

            // If one or both of them is negative that means the cursore left the game area.
            if (this.cursorX < 0 || this.cursorY < 0)
            {
                return;
            }

            // Check whether the value of speed would move the player in the desired direction.
            switch (direction)
            {
                case MovementDirection.Forward:
                    if (this.Model.Player.Speed < 0)
                    {
                        this.Model.Player.Speed *= -1;
                    }

                    break;
                case MovementDirection.Backwards:
                    if (this.Model.Player.Speed > 0)
                    {
                        this.Model.Player.Speed *= -1;
                    }

                    break;
            }

            this.CalculateVector(this.Model.Player);
            this.OutOfWindow(this.Model.Player);

            this.Model.Player.PositionX += this.Model.Player.StepX;
            this.Model.Player.CurrentWeapon.PositionX += this.Model.Player.StepX;
            this.Model.Player.PositionY += this.Model.Player.StepY;
            this.Model.Player.CurrentWeapon.PositionY += this.Model.Player.StepY;

            // Hitbox displacement :
            // CS1612 Cannot modify the return value of the hitbox because it is not a variable
            Size tmpSize = new Size(this.Model.Player.HitBox.Width, this.Model.Player.HitBox.Height);
            Point newLocation = new Point(this.Model.Player.PositionX, this.Model.Player.PositionY);
            Rect newHitBox = new Rect(newLocation, tmpSize);
            this.Model.Player.HitBox = newHitBox;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public void Shoot()
        {
            // If one or both of them is negative that means the cursore left the game area.
            if (cursorX < 0 || cursorY < 0)
            {
                return;
            }

            // +10 = offset or margin to say.
            PistolBullet weaponProj = new PistolBullet(
                this.Model.Player.CurrentWeapon.PositionX,
                this.Model.Player.CurrentWeapon.PositionY,
                this.Model.Player.CurrentWeapon.Projectile.Damage,
                this.Model.Player.CurrentWeapon.Projectile.Speed);

            this.CalculateVector(weaponProj);

            this.Model.Player.TrackedProjectiles.Add(weaponProj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="newIndex">New index of the selected weapon.</param>
        public void SwapWeapon(int newIndex)
        {
            this.Model.Player.CurrentWeapon = this.Model.Player.AllWeapons[newIndex];
        }

        // Helper functions

        /// <summary>
        /// Calculates the next movement vector for the passed objectToMove object relative to the Player.
        /// </summary>
        /// <param name="objectToMove"><see cref="IMovingObject">IMovingObject</see> object that we want to move.</param>
        private void CalculateVector(IMovingObject objectToMove)
        {
            if (objectToMove is not IProjectile)
            {
                // Displacement on the X and Y plan relative to the Player.
                double distanceX = this.cursorX - this.Model.Player.PositionX;
                double distanceY = this.cursorY - this.Model.Player.PositionY;
                double angle = Math.Atan2(distanceY, distanceX);    // Note: Angle is in rad.

                // Cartesian coordinates from the above polar variables.
                objectToMove.StepX = objectToMove.Speed * Math.Cos(angle);
                objectToMove.StepY = objectToMove.Speed * Math.Sin(angle);
            }
            else
            {
                // Displacement on the X and Y plan relative to the Player.
                double distanceX = this.cursorX - this.Model.Player.CurrentWeapon.PositionX;
                double distanceY = this.cursorY - this.Model.Player.CurrentWeapon.PositionY;
                double angle = Math.Atan2(distanceY, distanceX);    // Note: Angle is in rad.

                // Add some magic touch so the projectiles are not necessarily flying in a straight line.
                objectToMove.StepX = (objectToMove.Speed * Math.Cos(angle)) + rnd.NextDouble();
                objectToMove.StepY = (objectToMove.Speed * Math.Sin(angle)) + rnd.NextDouble();
            }

            return;
        }

        /// <summary>
        /// Evaluates the next step for an <see cref="IMovingObject">IMovingObject</see> object and checks whether the step would result to leave the game are or not.
        /// If it results in leaving the game area we simply set its step values to zero. Projectiles are also removed from the tracked collection.
        /// </summary>
        /// <param name="objectToMove"><see cref="IMovingObject">IMovingObject</see> object that we want to move.</param>
        private bool OutOfWindow(IMovingObject objectToMove)
        {
            double displacedX = objectToMove.PositionX + objectToMove.StepX;
            double displacedY = objectToMove.PositionY + objectToMove.StepY;

            // Hitbox displacement :
            // CS1612 Cannot modify the return value of the hitbox because it is not a variable
            Size tmpSize = new Size(this.Model.Player.HitBox.Width, this.Model.Player.HitBox.Height);
            Point newLocation = new Point(this.Model.Player.PositionX + this.Model.Player.StepX, this.Model.Player.PositionY + this.Model.Player.StepY);
            Rect newHitBox = new Rect(newLocation, tmpSize);

            // If it would lead out of the border we simply dont move.
            // Im not sure if it can happen tho... o.O
            // UGLY NUMBERS DO SOMETHING ABOUT IT LATER DANIEL!
            // UPDATE: Dont know why this still here but now Im just afraid to delete it :c
            if (displacedX >= 1280 || displacedY >= 720)
            {
                objectToMove.StepX = 0;
                objectToMove.StepY = 0;

                // If it is a projectile we simply untrack it by removing it from the collection.
                if (objectToMove is IProjectile)
                {
                    IProjectile projToRemove = objectToMove as IProjectile;
                    this.Model.Player.TrackedProjectiles.Remove(projToRemove);

                    return true;
                }
            }

            if (objectToMove is IPlayer)
            {
                if (newHitBox.IntersectsWith(this.Model.Nexus.HitBox))
                {
                    objectToMove.StepX = 0;
                    objectToMove.StepY = 0;
                }
            }

            return false;
        }

        private void DecidePlayerDirection(double degree)
        {
            // Upwards
            if (degree < 0)
            {
                if (degree > -22.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveRight)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveRight;
                    }
                }

                if (degree < -22.5 && degree > -67.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveRightDiagonalUp)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveRightDiagonalUp;
                    }
                }

                if (degree < -67.5 && degree > -112.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveUp)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveUp;
                    }
                }

                if (degree < -112.5 && degree > -157.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveLeftDiagonalUp)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveLeftDiagonalUp;
                    }
                }

                if (degree < -157.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveLeft)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveLeft;
                    }
                }
            }

            // Downwards
            else
            {
                if (degree < 22.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveRight)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveRight;
                    }
                }

                if (degree > 22.5 && degree < 67.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveRightDiagonalDown)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveRightDiagonalDown;
                    }
                }

                if (degree > 67.5 && degree < 112.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveDown)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveDown;
                    }
                }

                if (degree > 112.5 && degree < 157.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveLeftDiagonalDown)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveLeftDiagonalDown;
                    }
                }

                if (degree > 157.5)
                {
                    if (this.cash.MovementState != PlayerMovementState.MoveLeft)
                    {
                        this.Model.Player.MovementState = PlayerMovementState.MoveLeft;
                    }
                }
            }

            if (this.cash.MovementState != this.Model.Player.MovementState)
            {
                this.cash.IdleCount = 0;
            }
        }

        private void DecideWeaponDirection(double degree)
        {
            // Upwards
            if (degree < 0)
            {
                if (degree > -22.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Right;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Right][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Right][1];
                }

                if (degree < -22.5 && degree > -67.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.RightDiagonalUp;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.RightDiagonalUp][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.RightDiagonalUp][1];
                }

                if (degree < -67.5 && degree > -112.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Up;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Up][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Up][1];
                }

                if (degree < -112.5 && degree > -157.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.LeftDiagonalUp;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.LeftDiagonalUp][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.LeftDiagonalUp][1];
                }

                if (degree < -157.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Left;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Left][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Left][1];
                }
            }

            // Downwards
            else
            {
                if (degree < 22.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Right;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Right][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Right][1];
                }

                if (degree > 22.5 && degree < 67.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.RightDiagonalDown;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.RightDiagonalDown][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.RightDiagonalDown][1];
                }

                if (degree > 67.5 && degree < 112.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Down;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Down][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Down][1];
                }

                if (degree > 112.5 && degree < 157.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.LeftDiagonalDown;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.LeftDiagonalDown][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.LeftDiagonalDown][1];
                }

                if (degree > 157.5)
                {
                    this.Model.Player.CurrentWeapon.DirectionState = WeaponDirectionState.Left;
                    this.Model.Player.CurrentWeapon.PositionX = this.Model.Player.PositionX + weaponOffset[(int)WeaponDirectionState.Left][0];
                    this.Model.Player.CurrentWeapon.PositionY = this.Model.Player.PositionY + weaponOffset[(int)WeaponDirectionState.Left][1];
                }
            }
        }
    }
}
