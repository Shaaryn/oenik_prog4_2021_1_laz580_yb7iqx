﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="GameSceneLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.Logic.GameScene
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using AutoMapper;
    using SlimeShooter.GameLogic.GameScene.Helper;
    using SlimeShooter.GameLogic.MainMenuScene.Helper;
    using SlimeShooter.Logic.Interfaces.GameScene.Interfaces;
    using SlimeShooter.Models;
    using SlimeShooter.Models.States;
    using SlimeShooter.Models.Tiles;
    using SlimeShooter.Repositories;

    /// <summary>
    /// Logic class responsible for handling and managing the game scene as a whole.
    /// </summary>
    public sealed class GameSceneLogic : IGameSceneLogic
    {
        private string levelFilePath = "SlimeShooter.GameLogic..resources.Maps.map1.lvl";

        private IPlayerLogic playerLogic;
        private IEnemyLogic enemyLogic;
        private ModelCache cash;
        private PlayerRepository playerRepo;
        private IMapper mapper;

        /// <summary>
        /// Determines whether the game is over or not.
        /// </summary>
        public bool IsGameOver { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public IPlayerLogic PlayerLogic
        {
            get { return this.playerLogic; }
            set { this.playerLogic = value; }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public IEnemyLogic EnemyLogic
        {
            get { return this.enemyLogic; }
            set { this.enemyLogic = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameSceneLogic"/> class.
        /// </summary>
        /// <param name="model">Model containing the necessary models for the player and enemy entities.</param>
        /// <param name="playerRepo">Repository of the players.</param>
        public GameSceneLogic(GameModel model, PlayerRepository playerRepo)
        {
            if (model != null)
            {
                this.Model = model;
                this.cash = new ModelCache();
                this.playerLogic = new PlayerLogic(this.Model, cash);
                this.enemyLogic = new EnemyLogic(Model);
                this.IsGameOver = false;
                this.playerRepo = playerRepo;
                this.mapper = MapperFactory.CreateMapper();

                if (model.Map == null || model.Nexus == null)
                {
                    this.BuildMap();
                }

                this.enemyLogic.GenerateWave();
            }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        /// <param name="cursorX">Position of the cursor on the X axis.</param>
        /// <param name="cursorY">Position of the cursor on the Y axis.</param>
        public void ExecuteTick(double cursorX, double cursorY)
        {
            CheckIfIdle();
            this.playerLogic.Look(cursorX, cursorY);

            this.playerLogic.MoveProjectiles();
            this.enemyLogic.Move();

            this.CheckCollision();

            // Enemy wave control.
            if ((this.Model.EllapsedTime.ElapsedMilliseconds >= this.Model.MillisecondsTillNextWave) ||
                this.Model.SlimeMonsters.Count == 0)
            {
                if (this.Model.MillisecondsTillNextWave > 3000)
                {
                    // If all the enemies were annihilated and we have more then 3 secs left, we set it to have only 3 secs left.
                    this.Model.MillisecondsTillNextWave = 3000;

                    // Also gonna need to restart the timer.
                    this.Model.EllapsedTime.Restart();
                }

                if (this.Model.EllapsedTime.ElapsedMilliseconds >= this.Model.MillisecondsTillNextWave)
                {
                    this.enemyLogic.GenerateWave();
                    this.Model.MillisecondsTillNextWave = 15000 - (int)Math.Ceiling(Math.Log2((double)this.Model.EnemyWave) * 1000);
                }
            }
        }

        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public void SaveScore()
        {
            IQueryable<Data.Entities.Player> leaderboard = playerRepo.GetAll();
            bool isHigher = false;
            Data.Entities.Player dataPlayer = new Data.Entities.Player();
            foreach (Data.Entities.Player player in leaderboard)
            {
                if (player.Id == this.Model.Player.Id)
                {
                    if (player.Score <= Model.Player.Score)
                    {
                        player.Score = this.Model.Player.Score;
                        dataPlayer = player;
                        isHigher = true;
                    }
                }
            }

            if (isHigher)
            {
                this.playerRepo.Update(Model.Player.Id, dataPlayer);
            }
        }

        /// <summary>
        /// Checks for collisions and takes the necessary steps afterwards if needed.
        /// </summary>
        private void CheckCollision()
        {
            CheckNexusProjectileCollision();

            CheckEnemyProjectileCollision();

            CheckEnemyNexusCollision();

            CheckEnemyPlayerCollision();
        }

        private void CheckNexusProjectileCollision()
        {
            List<int> projectilesToRemove = new List<int>();

            for (int i = 0; i < this.Model.Player.TrackedProjectiles.Count; i++)
            {
                if (this.Model.Nexus.HitBox.IntersectsWith(this.Model.Player.TrackedProjectiles[i].HitBox))
                {
                    projectilesToRemove.Add(i);
                }
            }

            for (int i = 0; i < projectilesToRemove.Count; i++)
            {
                this.Model.Player.TrackedProjectiles.RemoveAt(projectilesToRemove[i] - i);
            }
        }

        private void CheckEnemyProjectileCollision()
        {
            List<int> projectilesToRemove = new List<int>();
            List<int> enemiesToRemove = new List<int>();

            // Collect the projetiles that hit an enemy.
            for (int i = 0; i < this.Model.Player.TrackedProjectiles.Count; i++)
            {
                for (int j = 0; j < this.Model.SlimeMonsters.Count; j++)
                {
                    if (this.Model.Player.TrackedProjectiles[i].HitBox.IntersectsWith(
                        this.Model.SlimeMonsters[j].HitBox))
                    {
                        enemyLogic.SufferDamage(this.Model.SlimeMonsters[j], this.Model.Player.TrackedProjectiles[i]);
                        if (!projectilesToRemove.Contains(i))
                        {
                            projectilesToRemove.Add(i);
                        }

                        // If the enemy died we remvoe it later.
                        if (this.Model.SlimeMonsters[j].HealtPoints <= 0)
                        {
                            enemiesToRemove.Add(j);
                        }
                    }
                }
            }

            // Remove the projectiels that hit an enemy.
            for (int i = 0; i < projectilesToRemove.Count; i++)
            {
                this.Model.Player.TrackedProjectiles.RemoveAt(projectilesToRemove[i] - i);
            }

            // Remove the dead enemies
            for (int i = 0; i < enemiesToRemove.Count; i++)
            {
                this.Model.SlimeMonsters.RemoveAt(enemiesToRemove[i] - i);
            }
        }

        private void CheckEnemyNexusCollision()
        {
            List<int> enemiesToRemove = new List<int>();

            // Collect the enemies that reached the nexus.
            for (int i = 0; i < this.Model.SlimeMonsters.Count; i++)
            {
                if (this.Model.SlimeMonsters[i].HitBox.IntersectsWith(this.Model.Nexus.HitBox))
                {
                    this.enemyLogic.DealDamage(this.Model.Nexus, this.Model.SlimeMonsters[i]);
                    enemiesToRemove.Add(i);
                }
            }

            // Remvoe the enemies that reached the nexus.
            for (int i = 0; i < enemiesToRemove.Count; i++)
            {
                this.Model.SlimeMonsters.RemoveAt(enemiesToRemove[i] - i);
            }

            if (this.Model.Nexus.HealtPoints <= 0)
            {
                SaveScore();
                this.IsGameOver = true;
            }
        }

        private void CheckEnemyPlayerCollision()
        {
            List<int> enemiesToRemove = new List<int>();

            for (int i = 0; i < this.Model.SlimeMonsters.Count; i++)
            {
                if (this.Model.SlimeMonsters[i].HitBox.IntersectsWith(this.Model.Player.HitBox))
                {
                    this.enemyLogic.DealDamage(this.Model.Nexus, this.Model.SlimeMonsters[i]);
                    enemiesToRemove.Add(i);
                }
            }

            // Remvoe the enemies that reached the nexus.
            for (int i = 0; i < enemiesToRemove.Count; i++)
            {
                this.Model.SlimeMonsters.RemoveAt(enemiesToRemove[i] - i);
            }

            if (this.Model.Nexus.HealtPoints <= 0)
            {
                SaveScore();
                this.IsGameOver = true;
            }
        }

        private bool CheckIfIdle()
        {
            if (this.cash.PlayerPositionX == this.Model.Player.PositionX &&
                this.cash.PlayerPositionY == this.Model.Player.PositionY &&
                this.cash.MovementState == this.Model.Player.MovementState)
            {
                // hehe még kiderül
                if (this.cash.IdleCount < 1)
                {
                    this.cash.IdleCount++;
                    return true;
                }
                else
                {
                    this.Model.Player.MovementState = PlayerMovementState.Idle;
                    return true;
                }
            }
            else
            {
                this.cash.IdleCount = 0;
            }

            return false;
        }

        private void BuildMap()
        {
            switch (this.Model.GameSettings.MapType)
            {
                case MapType.Random:

                    GenerateRandomMap();

                    break;
                case MapType.File:

                    GenerateFileMap();

                    break;
            }
        }

        private void GenerateRandomMap()
        {
            string[][] map = new string[11][];
            MapGenerator generator = new MapGenerator(map, this.Model.GameSettings.Difficulty);

            map = generator.Generate();
            this.Model.Map = new Tile[11, 20];

            for (int i = 0; i < map.Length; i++)
            {
                for (int j = 0; j < map[i].Length; j++)
                {
                    if (map[i][j] == "P")
                    {
                        this.Model.Map[i, j] = new DirtTile(0, 0);
                        if (this.Model.Portals.Count == 0)
                        {
                            this.Model.Portals.Add((new Portal(false, (i * 64), (j * 64))), true);
                        }
                        else
                        {
                            this.Model.Portals.Add((new Portal(false, (i * 64), (j * 64))), false);
                        }
                    }
                    else if (map[i][j] == "x")
                    {
                        this.Model.Map[i, j] = new DirtTile(0, 0);
                    }
                    else if (map[i][j] == "A")
                    {
                        this.Model.Map[i, j] = new DirtTile(0, 0);
                    }
                    else if (map[i][j] == "N")
                    {
                        this.Model.Map[i, j] = new GrassTile(0, 0);
                        this.Model.Nexus = new Nexus(j * 64, i * 64, 375);
                    }
                    else
                    {
                        this.Model.Map[i, j] = new GrassTile(0, 0);
                    }
                }
            }
        }

        private void GenerateFileMap()
        {
            this.Model.Map = new Tile[11, 20];
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(this.levelFilePath);
            StreamReader sr = new StreamReader(stream);

            string[][] allLines = new string[11][];
            int n = 0;
            do
            {
                string[] line;
                line = sr.ReadLine().Split("|");
                allLines[n] = line;
                n++;
            }
            while (!sr.EndOfStream);

            for (int i = 0; i < allLines.Length; i++)
            {
                for (int j = 0; j < allLines[i].Length; j++)
                {
                    if (allLines[i][j] == "x")
                    {
                        this.Model.Map[i, j] = new DirtTile(0, 0);

                        // this.Model.Map[i, i] = new GrassTile(0, 0);
                    }
                    else
                    {
                        this.Model.Map[i, j] = new GrassTile(0, 0);
                    }
                }
            }

            // -70 = (Nexus.width / 2), -66 = (Nexus.height / 2)
            this.Model.Nexus = new Nexus((1280 / 2) - 70, (720 / 2) - 66, 375);

            sr.Dispose();
        }
    }
}
