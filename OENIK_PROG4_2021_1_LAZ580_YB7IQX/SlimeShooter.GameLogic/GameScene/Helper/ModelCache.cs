﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="ModelCache.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.GameScene.Helper
{
    using SlimeShooter.Models.States;

    /// <summary>
    /// Helper class responsible of storing the cash data.
    /// </summary>
    public class ModelCache
    {
        /// <summary>
        /// Counts how many ticks ellapsed since the last movement.
        /// </summary>
        public int IdleCount { get; set; }

        /// <summary>
        /// Cashes the player's position on the X axis.
        /// </summary>
        public double PlayerPositionX { get; set; }

        /// <summary>
        /// Cashes the player's position on the Y axis.
        /// </summary>
        public double PlayerPositionY { get; set; }

        /// <summary>
        /// Cashes the player's movement state.
        /// </summary>
        public PlayerMovementState MovementState { get; set; }
    }
}
