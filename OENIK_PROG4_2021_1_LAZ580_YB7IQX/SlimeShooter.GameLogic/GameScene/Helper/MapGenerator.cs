﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="MapGenerator.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.GameScene.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SlimeShooter.Models.States;

    /// <summary>
    /// Generates a random map.
    /// </summary>
    public class MapGenerator
    {
        private const int MINSTEP = 30;
        private static Random rnd = new Random();

        private string[][] map;

        // Route variables
        private (int y, int x) startPosition;      // Position of the "entrance"
        private (int y, int x) currentPosition;    // Current point we are evaluating.
        private Dictionary<RouteDirection, bool> isPossibleStep;
        private List<RouteDirection> finalPossibilities;
        private List<RouteDirection> decisionLog;        // Log used to avoid too many repetitions.
        private int stepNumber;
        private bool isOutOfBounds;
        private bool isLongEnough;

        // Portal variables
        private Difficulty difficulty;      // Difficulty of the game. Determines the number of portals we have.
        private int chance;     // The higher the value the more likely it will happen to place a portal.
        private int chanceStep;     // With how many percentage we increment the chance in each round to get to place a portal on the maxStep.
        private int maxSteps;       // How many steps can be done without placing a portal.
        private int skipRounds;
        private bool isPortal;      // Do we place a portal in the current step?
        private int placedPortals;

        // Nexus variables
        private Dictionary<RouteSector, bool> containsPortal;       // Knows whether we have a portal placed in the given sector or not.
        private (int y, int x) centerPoint;
        private int usedSectors;
        private List<RouteSector> freeSectors;
        private Dictionary<RouteSector, (int y, int x)> possibleNexusPositions;
        private bool isGoodNexus;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapGenerator"/> class.
        /// </summary>
        /// <param name="map">Property responsible for storing the map.</param>
        /// <param name="chosenDifficulty">Difficulty that the player chose in the main menu.</param>
        public MapGenerator(string[][] map, Difficulty chosenDifficulty)
        {
            this.map = map;
            this.difficulty = chosenDifficulty;
        }

        /// <summary>
        /// In this function we see the flow of the algorithm.
        /// There are 2 points where the generation can fail, if it is not long and curly enough or if the nexus is can't be placed.
        /// In either error occuring the generation starts over again.
        /// </summary>
        /// <returns>Jagged array that contains the map.</returns>
        public string[][] Generate()
        {
            do
            {
                GenerateEmptyMap();
                Initialization();

                do
                {
                    CheckDirections();
                    GenerateOptionsPool();
                    MakeDecision();
                    if (isOutOfBounds)
                    {
                        isLongEnough = true;
                    }
                }
                while (!isLongEnough);

                if (!isOutOfBounds)
                {
                    NexusPlacement();
                }
            }
            while (!isGoodNexus);

            return this.map;
        }

        /// <summary>
        /// In this function we simply set the initial values for each variable, list or enum or whatever.
        /// This became necessary because the algorithm is not meant to be reliable.
        /// However it can decide whether it successfully completed its job based on the given rules or not.
        /// And if not, it will start the whole thing over again which is the reason we cant have these stuf in the ctor only.
        /// </summary>
        private void Initialization()
        {
            // Route variables
            startPosition = (0, 9);
            currentPosition = startPosition;
            isPossibleStep = new Dictionary<RouteDirection, bool>();
            finalPossibilities = new List<RouteDirection>();
            decisionLog = new List<RouteDirection>();
            stepNumber = 1;
            isLongEnough = false;
            isOutOfBounds = false;

            Array directions = Enum.GetNames(typeof(RouteDirection));
            foreach (string item in directions)
            {
                RouteDirection currentDir = (RouteDirection)Enum.Parse(typeof(RouteDirection), item);
                isPossibleStep.Add(currentDir, false);
            }

            // Portal variables
            skipRounds = 0;
            isPortal = false;
            placedPortals = 0;
            chance = 0;
            chanceStep = 0;

            // Nexus variables
            containsPortal = new Dictionary<RouteSector, bool>();
            centerPoint = (((this.map.Length - 1) / 2), ((this.map[0].Length - 1) / 2));
            usedSectors = 0;
            freeSectors = new List<RouteSector>();
            possibleNexusPositions = new Dictionary<RouteSector, (int y, int x)>();
            isGoodNexus = false;

            Array sectors = Enum.GetNames(typeof(RouteSector));
            foreach (string item in sectors)
            {
                RouteSector currentSec = (RouteSector)Enum.Parse(typeof(RouteSector), item);
                containsPortal.Add(currentSec, false);
            }

            possibleNexusPositions.Add(RouteSector.UpLeft, (2, 2));
            possibleNexusPositions.Add(RouteSector.UpRight, (2, this.map[0].Length - 3));
            possibleNexusPositions.Add(RouteSector.BotRight, ((this.map.Length - 3), (this.map[0].Length - 3)));
            possibleNexusPositions.Add(RouteSector.BotLeft, ((this.map.Length - 3), 2));
        }

        /// <summary>
        /// In this function we simply fill up a n*n jagged array with "o"-s to represent an empty map.
        /// </summary>
        private void GenerateEmptyMap()
        {
            for (int i = 0; i < map.Length; i++)
            {
                string[] tmp = new string[20];
                for (int j = 0; j < tmp.Length; j++)
                {
                    if ((i, j) == startPosition)
                    {
                        tmp[j] = "A";
                    }
                    else
                    {
                        tmp[j] = "o";
                    }
                }

                map[i] = tmp;
            }
        }

        /// <summary>
        /// In this function we mostly take care about the repetitions. Well we are not taking care of them, rather we control them.
        /// We oversee the list o fpossibilities by looking up their occurances in the previous x steps.
        /// If a direction has been used a lot in the previous x steps, we remove it from the possibilities.
        /// Another important part is that we make sure that the road "moves". By this I mean, we prefer to have the road spread, by ensuring it moves dwonwards from time to time.
        /// </summary>
        private void GenerateOptionsPool()
        {
            this.finalPossibilities = isPossibleStep.Where(x => x.Value == true).Select(x => x.Key).ToList();

            // If it is not the first step and we have more than 1 direction to choose from.
            if (decisionLog.Count != 0 && finalPossibilities.Count != 1)
            {
                // If it is the second step
                if (decisionLog.Count == 1)
                {
                    // If the second step could be the first step we allow a 50% chance to still be considered.
                    if (finalPossibilities.Contains(decisionLog[0]))
                    {
                        if (rnd.Next(0, 100) < 50)
                        {
                            finalPossibilities.Remove(decisionLog[0]);
                        }
                    }
                }
                else
                {
                    // We need at least 2 elements for this decision.
                    if (decisionLog.Count >= 2 && finalPossibilities.Count != 1)
                    {
                        // If our previous decision is among the possibilities again.
                        if (finalPossibilities.Contains(decisionLog[^1]))
                        {
                            // If our previous decision IS the same as the decision before the previous.
                            if (decisionLog[^2] == decisionLog[^1])
                            {
                                // We 100% remove this possibility, hence preventing to have 2+ repetitive directions.
                                finalPossibilities.Remove(decisionLog[^1]);
                            }

                            // If our previous decision IS NOT the same as the decision before the previous.
                            else
                            {
                                // We allow a 50-50 chance for the controlled repetition to happen.
                                if (rnd.Next(0, 100) < 50)
                                {
                                    finalPossibilities.Remove(decisionLog[^1]);
                                }
                            }
                        }
                    }

                    // We need at least 3 elements for this decision.
                    if (decisionLog.Count >= 3)
                    {
                        // !!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!
                        // Ezt az if-et szedd ki ha szeretnél teljesen véletlenszerű össze-vissza mappot és nem lefelé irányulót!
                        // !!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!-----!!!!!
                        // If our previous two steps were not down steps.
                        if ((decisionLog[^1] != RouteDirection.Down) && (decisionLog[^2] != RouteDirection.Down))
                        {
                            // If even the 3rd previous was not down, we make it the only possible next step.
                            if ((decisionLog[^3] != RouteDirection.Down))
                            {
                                finalPossibilities.Clear();
                                finalPossibilities.Add(RouteDirection.Down);
                            }
                            else
                            {
                                Dictionary<RouteDirection, int> usageCount = new Dictionary<RouteDirection, int>();
                                Array directions = Enum.GetNames(typeof(RouteDirection));
                                foreach (string item in directions)
                                {
                                    RouteDirection currentDirection = (RouteDirection)Enum.Parse(typeof(RouteDirection), item);
                                    usageCount.Add(currentDirection, 0);
                                }

                                // We eliminate the most used direction to increase the chance of a down step.
                                if (decisionLog.Count < 4)
                                {
                                    // Count occurances of the directions.
                                    for (int i = 0; i < decisionLog.Count; i++)
                                    {
                                        usageCount[decisionLog[i]] += 1;
                                    }
                                }
                                else
                                {
                                    // Count occurances of the directions. Back until the previous 4.
                                    for (int i = 0; i < 4; i++)
                                    {
                                        usageCount[decisionLog[i]] += 1;
                                    }
                                }

                                // Sort by values (count).
                                var sortedList = usageCount.ToList();
                                sortedList.Sort((p1, p2) => p1.Value.CompareTo(p2.Value));

                                // We remove the most used direction to increase the chance of the down direction.
                                finalPossibilities.Remove(sortedList[0].Key);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// In this function, based on all the available informations and logs and all we make a decision.
        /// We handle the possibilities and choose one either randomly or by "force" (corners).
        /// After we made this decision we either put a portal on the next tile in that direction or not.
        /// According to the portal decision we mark the location with its corresponding tile.
        /// </summary>
        private void MakeDecision()
        {
            int x;
            int y;
            RouteDirection decision = default(RouteDirection);

            // If there are no possibilities and we have gone far enough we call it a day.
            if (finalPossibilities.Count == 0 && stepNumber >= MINSTEP)
            {
                isLongEnough = true;
                return;
            }

            // If we didn't stepped at least MINSTEP times.
            else
            {
                // And if we dont have any possibilities to choose from that means we are in a corner.
                if (finalPossibilities.Count == 0)
                {
                    decision = HandleCorner();
                }

                // If we are not in a corner, we simply continue by choosing a random direction from the possibilities.
                else
                {
                    decision = finalPossibilities[rnd.Next(0, finalPossibilities.Count)];
                }
            }

            // Lets decide the tile's fate.
            PortalPlacement();

            switch (decision)
            {
                case RouteDirection.Left:
                    x = currentPosition.x - 1;
                    y = currentPosition.y;

                    if (((x < 0) || (x > map[0].Length - 1)) ||
                        ((y < 0) || (y > map.Length - 1)))
                    {
                        isOutOfBounds = true;
                    }
                    else
                    {
                        if (isPortal)
                        {
                            map[y][x] = "P";
                            isPortal = false;

                            // We note that we have placed a portal in this sector
                            NotePlacedPortal();
                        }
                        else
                        {
                            // Map[y][x] = stepNumber.ToString();
                            map[y][x] = "x";
                        }

                        currentPosition = (y, x);
                        stepNumber++;
                    }

                    decisionLog.Add(decision);
                    break;
                case RouteDirection.Up:
                    x = currentPosition.x;
                    y = currentPosition.y - 1;

                    if (((x < 0) || (x > map[0].Length - 1)) ||
                        ((y < 0) || (y > map.Length - 1)))
                    {
                        isOutOfBounds = true;
                    }
                    else
                    {
                        if (isPortal)
                        {
                            map[y][x] = "P";
                            isPortal = false;

                            // We note that we have placed a portal in this sector
                            NotePlacedPortal();
                        }
                        else
                        {
                            // Map[y][x] = stepNumber.ToString();
                            map[y][x] = "x";
                        }

                        currentPosition = (y, x);
                        stepNumber++;
                    }

                    decisionLog.Add(decision);
                    break;
                case RouteDirection.Right:
                    x = currentPosition.x + 1;
                    y = currentPosition.y;

                    if (((x < 0) || (x > map[0].Length - 1)) ||
                        ((y < 0) || (y > map.Length - 1)))
                    {
                        isOutOfBounds = true;
                    }
                    else
                    {
                        if (isPortal)
                        {
                            map[y][x] = "P";
                            isPortal = false;

                            // We note that we have placed a portal in this sector
                            NotePlacedPortal();
                        }
                        else
                        {
                            // Map[y][x] = stepNumber.ToString();
                            map[y][x] = "x";
                        }

                        currentPosition = (y, x);
                        stepNumber++;
                    }

                    decisionLog.Add(decision);
                    break;
                case RouteDirection.Down:
                    x = currentPosition.x;
                    y = currentPosition.y + 1;

                    if (((x < 0) || (x > map[0].Length - 1)) ||
                        ((y < 0) || (y > map.Length - 1)))
                    {
                        isOutOfBounds = true;
                    }
                    else
                    {
                        if (isPortal)
                        {
                            map[y][x] = "P";
                            isPortal = false;

                            // We note that we have placed a portal in this sector
                            NotePlacedPortal();
                        }
                        else
                        {
                            // Map[y][x] = stepNumber.ToString();
                            map[y][x] = "x";
                        }

                        currentPosition = (y, x);
                        stepNumber++;
                    }

                    decisionLog.Add(decision);
                    break;
            }
        }

        /// <summary>
        /// In this function we decide whether we place a portal or not. Base on the given difficulty the number of necessary portals are different.
        /// This function makes sure that there is a minimum x step-distance between 2 portals, while also maintaining the maximum amount of steps when a portal must be placed.
        /// This is done by the chance variable that is increasing by a predfined amount each time we dont place a portal, hence innevitably leading to a protal placement after x steps.
        /// </summary>
        private void PortalPlacement()
        {
            if (chance != -1)
            {
                // If it is the first step we set the chance variables
                if (stepNumber == 1)
                {
                    maxSteps = MINSTEP / (int)difficulty;
                    chanceStep = (int)Math.Ceiling((double)(100 / maxSteps));
                    chance = chanceStep;
                }
                else
                {
                    // If we are not supposed to miss this round, we evaluate.
                    if (skipRounds == 0)
                    {
                        // If we placed all the portals we wanted, we simply skip this whole function from now on.
                        if (placedPortals == (int)difficulty)
                        {
                            chance = -1;
                        }

                        // We place a portal if we are inside the chance.
                        else if (rnd.Next(0, 100) < chance)
                        {
                            isPortal = true;
                            placedPortals++;

                            // We miss "some" rounds based on the maxSteps variable to avoid adjacent portals.
                            skipRounds = (int)(Math.Floor((double)(maxSteps / 2)) - 1);

                            // We set back the chance to the initial value.
                            chance = chanceStep;
                        }

                        // If nothing, we increase the possibility of palcing a portal next time.
                        else
                        {
                            chance += chanceStep;
                        }
                    }

                    // If we placed a portal recently we skip this round and decrement the corresponding counter.
                    else
                    {
                        skipRounds--;
                    }
                }
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// In this functions we note that we have placed a portal in a sector.
        /// Which will be used to determine the place of the nexus later.
        /// </summary>
        private void NotePlacedPortal()
        {
            // It's in the Left side.
            if (currentPosition.x <= centerPoint.x)
            {
                // It's in the Upper Left sector.
                if (currentPosition.y <= centerPoint.y)
                {
                    containsPortal[RouteSector.UpLeft] = true;
                }

                // It's in the Lower Left sector.
                else
                {
                    containsPortal[RouteSector.BotLeft] = true;
                }
            }

            // It's on the right side
            else
            {
                // It's in the Upper Right sector.
                if (currentPosition.y <= centerPoint.y)
                {
                    containsPortal[RouteSector.UpRight] = true;
                }

                // It's in the Lower Right sector.
                else
                {
                    containsPortal[RouteSector.BotRight] = true;
                }
            }
        }

        /// <summary>
        /// In this function we look through our portal log (containsPortal dictionary) and gather the portal-free sectors.
        /// Based on the amount of free sectors we have, we decide (or "gamble") the place of the nexus.
        /// </summary>
        private void NexusPlacement()
        {
            // Collecting the sectors that does not have any portals inside.
            ICollection<RouteSector> keyCollection = containsPortal.Keys;
            foreach (RouteSector item in keyCollection)
            {
                if (containsPortal[item])
                {
                    usedSectors++;
                }
                else
                {
                    freeSectors.Add(item);
                }
            }

            // If there are no free sectors we simply return, thus initiating the whole process again from the beginning.
            if (freeSectors.Count == 0)
            {
                return;
            }

            // If we have all the portals in the same sector.
            else if (freeSectors.Count == 3)
            {
                // We simply put it to the diagonal opposite.
                switch (containsPortal.Where(x => x.Value == true).Select(x => x.Key).FirstOrDefault())
                {
                    case RouteSector.UpLeft:
                        map[possibleNexusPositions[RouteSector.BotRight].y]
                           [possibleNexusPositions[RouteSector.BotRight].x] = "N";
                        break;
                    case RouteSector.UpRight:
                        map[possibleNexusPositions[RouteSector.BotLeft].y]
                           [possibleNexusPositions[RouteSector.BotLeft].x] = "N";
                        break;
                    case RouteSector.BotRight:
                        map[possibleNexusPositions[RouteSector.UpLeft].y]
                           [possibleNexusPositions[RouteSector.UpLeft].x] = "N";
                        break;
                    case RouteSector.BotLeft:
                        map[possibleNexusPositions[RouteSector.UpRight].y]
                           [possibleNexusPositions[RouteSector.UpRight].x] = "N";
                        break;
                }
            }

            // If the portals are more widespread.
            else
            {
                switch (freeSectors.Count)
                {
                    // We put it in the only free sector.
                    case 1:
                        map[possibleNexusPositions[freeSectors[0]].y][possibleNexusPositions[freeSectors[0]].x] = "N";
                        break;

                    // We "toss a coin" to decide in which sector we want to place the nexus.
                    case 2:
                        if (rnd.Next(0, 100) < 50)
                        {
                            map[possibleNexusPositions[freeSectors[0]].y]
                               [possibleNexusPositions[freeSectors[0]].x] = "N";
                        }
                        else
                        {
                            map[possibleNexusPositions[freeSectors[1]].y]
                               [possibleNexusPositions[freeSectors[1]].x] = "N";
                        }

                        break;
                }
            }

            isGoodNexus = true;
            return;
        }

        /// <summary>
        /// In this functions we handle the special possibilities, namely the corners.
        /// Corners are specials because the system would not recognise any possible direction to move forward to.
        /// It is becuase we only have one possibility which does not meet the requirements of the CheckDirections().
        /// That is beacuse, even the only possible direction has a "neighbor", which is the void, that is off the map.
        /// So we set these 4 possibilities by hard typing them.
        /// </summary>
        /// <returns>The only possible direction from the specific corner.</returns>
        private RouteDirection HandleCorner()
        {
            // If we are in the UpLeft corrner.
            if (currentPosition == (0, 0))
            {
                // If we came from left.
                if (decisionLog[^1] == RouteDirection.Left)
                {
                    return RouteDirection.Down;
                }

                // If we came from above.
                else if (decisionLog[^1] == RouteDirection.Up)
                {
                    return RouteDirection.Right;
                }
            }

            // If we are in the UpRight corner.
            else if (currentPosition == (0, (map[0].Length - 1)))
            {
                // If we came from right.
                if (decisionLog[^1] == RouteDirection.Right)
                {
                    return RouteDirection.Down;
                }

                // If we came from above.
                else if (decisionLog[^1] == RouteDirection.Up)
                {
                    return RouteDirection.Left;
                }
            }

            // If we are in the BotRight corner.
            else if (currentPosition == ((map.Length - 1), (map[0].Length - 1)))
            {
                // If we came from right.
                if (decisionLog[^1] == RouteDirection.Right)
                {
                    return RouteDirection.Up;
                }

                // If we came from below.
                else if (decisionLog[^1] == RouteDirection.Down)
                {
                    return RouteDirection.Left;
                }
            }

            // If we are in the BotLeft corner.
            else if (currentPosition == ((map.Length - 1), 0))
            {
                // If we came from left.
                if (decisionLog[^1] == RouteDirection.Left)
                {
                    return RouteDirection.Up;
                }

                // If we came from below.
                else if (decisionLog[^1] == RouteDirection.Down)
                {
                    return RouteDirection.Right;
                }
            }

            // Not gonna be good but we shouldnt even get here I think.
            return default(RouteDirection);
        }

        /// <summary>
        /// In this function we simply initiate a self check routine for each direction we could move to.
        /// The self checks return their evaluation result and we store that in a dictionary corresponding to their direction.
        /// </summary>
        private void CheckDirections()
        {
            ICollection<RouteDirection> keyCollection = isPossibleStep.Keys;

            foreach (RouteDirection item in keyCollection.ToList())
            {
                switch (item)
                {
                    case RouteDirection.Left:
                        isPossibleStep[item] = CheckLeft();
                        break;
                    case RouteDirection.Up:
                        isPossibleStep[item] = CheckUp();
                        break;
                    case RouteDirection.Right:
                        isPossibleStep[item] = CheckRight();
                        break;
                    case RouteDirection.Down:
                        isPossibleStep[item] = CheckDown();
                        break;
                }
            }
        }

        /// <summary>
        /// In this function we pretend that we have steped one left.
        /// While pretending this step we seek the answer for the following things:
        /// - Would we leave the map?
        /// - Would we step on an occupied (not "o") place?
        /// - Would we have any neighbor(s) (not "o") on our sides (not counting the position where we came from)?.
        /// </summary>
        /// <returns>The answer to the questions above. Only true if non of the above is true.</returns>
        private bool CheckLeft()
        {
            int x = currentPosition.x - 1;

            // Check whether we would leave the map or not.
            if (x < 0)
            {
                return false;
            }

            // Check whether the proposed place is empty or not.
            if (map[currentPosition.y][x] != "o")
            {
                return false;
            }
            else
            {
                // Upper neighbor if we move left.
                if ((currentPosition.y + 1) <= (map.Length - 1))
                {
                    if (map[currentPosition.y + 1][x] != "o")
                    {
                        return false;
                    }
                }

                // Left neighbor if we move left.
                if ((x - 1) >= 0)
                {
                    if (map[currentPosition.y][x - 1] != "o")
                    {
                        return false;
                    }
                }

                // Bottom neighbor if we move left.
                if ((currentPosition.y - 1) >= 0)
                {
                    if (map[currentPosition.y - 1][x] != "o")
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// In this function we pretend that we have steped one upwards.
        /// While pretending this step we seek the answer for the following things:
        /// - Would we leave the map?
        /// - Would we step on an occupied (not "o") place?
        /// - Would we have any neighbor(s) (not "o") on our sides (not counting the position where we came from)?.
        /// </summary>
        /// <returns>The answer to the questions above. Only true if non of the above is true.</returns>
        private bool CheckUp()
        {
            int y = currentPosition.y - 1;

            // Check whether we would leave the map or not.
            if (y < 0)
            {
                return false;
            }

            // Check whether the proposed place is empty or not.
            if (map[y][currentPosition.x] != "o")
            {
                return false;
            }
            else
            {
                // Left neighbor if we move up.
                if ((currentPosition.x - 1) >= 0)
                {
                    if (map[y][currentPosition.x - 1] != "o")
                    {
                        return false;
                    }
                }

                // Upper neighbor if we move up.
                if ((y - 1) >= (map.Length - 1))
                {
                    if (map[y - 1][currentPosition.x] != "o")
                    {
                        return false;
                    }
                }

                // Right neighbor if we move up.
                if ((currentPosition.x + 1) <= (map[0].Length - 1))
                {
                    if (map[y][currentPosition.x + 1] != "o")
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// In this function we pretend that we have steped one right.
        /// While pretending this step we seek the answer for the following things:
        /// - Would we leave the map?
        /// - Would we step on an occupied (not "o") place?
        /// - Would we have any neighbor(s) (not "o") on our sides (not counting the position where we came from)?.
        /// </summary>
        /// <returns>The answer to the questions above. Only true if non of the above is true.</returns>
        private bool CheckRight()
        {
            int x = currentPosition.x + 1;

            // Check whether we would leave the map or not.
            if (x > (map.Length - 1))
            {
                return false;
            }

            // Check whether the proposed place is empty or not.
            if (map[currentPosition.y][x] != "o")
            {
                return false;
            }
            else
            {
                // Upper neighbor if we move right.
                if ((currentPosition.y - 1) >= 0)
                {
                    if (map[currentPosition.y - 1][x] != "o")
                    {
                        return false;
                    }
                }

                // Right neighbor if we move right.
                if ((x + 1) <= (map[0].Length - 1))
                {
                    if (map[currentPosition.y][x + 1] != "o")
                    {
                        return false;
                    }
                }

                // Bottom neighbor if we move right.
                if ((currentPosition.y + 1) <= (map.Length - 1))
                {
                    if (map[currentPosition.y + 1][x] != "o")
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// In this function we pretend that we have steped one downwards.
        /// While pretending this step we seek the answer for the following things:
        /// - Would we leave the map?
        /// - Would we step on an occupied (not "o") place?
        /// - Would we have any neighbor(s) (not "o") on our sides (not counting the position where we came from)?.
        /// </summary>
        /// <returns>The answer to the questions above. Only true if non of the above is true.</returns>
        private bool CheckDown()
        {
            int y = currentPosition.y + 1;

            // Check whether we would leave the map or not.
            if (y > (map.Length - 1))
            {
                return false;
            }

            // Check whether the proposed place is empty or not.
            if (map[y][currentPosition.x] != "o")
            {
                return false;
            }
            else
            {
                // Left neighbor if we move down.
                if ((currentPosition.x - 1) >= 0)
                {
                    if (map[y][currentPosition.x - 1] != "o")
                    {
                        return false;
                    }
                }

                // Bottom neighbor if we move down.
                if ((y + 1) <= 0)
                {
                    if (map[y + 1][currentPosition.x] != "o")
                    {
                        return false;
                    }
                }

                // Right neighbor if we move down.
                if ((currentPosition.x + 1) <= (map[0].Length - 1))
                {
                    if (map[y][currentPosition.x + 1] != "o")
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
