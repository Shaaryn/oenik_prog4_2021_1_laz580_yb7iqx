﻿// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|
// <copyright file="PauseLogic.cs" company="Supreme Game Studios">
// Copyright (c) Supreme Game Studios. All rights reserved.
// </copyright>
// |+|-----------------LAZ580--------------------YB7IQX-----------------|+|

namespace SlimeShooter.GameLogic.GameScene
{
    using System.IO;
    using System.Xml.Linq;
    using SlimeShooter.Models;
    using SlimeShooter.Models.Tiles;

    /// <summary>
    /// Logic class responsible for handling the pause scene.
    /// </summary>
    public class PauseLogic
    {
        /// <summary>
        /// <inheritdoc/>.
        /// </summary>
        public GameModel Model { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PauseLogic"/> class.
        /// </summary>
        /// <param name="model"><see cref="GameModel"/> that holds data regarding the game.</param>
        public PauseLogic(GameModel model)
        {
            this.Model = model;
        }

        /// <summary>
        /// Saves the current progress of the player into a .sav file using XML.
        /// Saves the following properties :
        /// ---Player(name, posX, posY, score)
        /// ---Settings(MapType, Difficulty)
        /// ---Nexus(posX, posY, HP).
        /// </summary>
        public void SaveGame()
        {
            // string path = Directory.GetCurrentDirectory() + $"\\saves\\save.sav";
            string path = @"..\..\..\..\SlimeShooter.GameLogic\.resources\Saves\save.sav";
            XDocument xdoc = new XDocument();

            XElement rootNode = new XElement("SlimeShooter");

            // Player node
            XElement playerNode = new XElement(
                nameof(this.Model.Player),
                new XAttribute(
                    nameof(this.Model.Player.Id),
                    this.Model.Player.Id),
                new XAttribute(
                    nameof(this.Model.Player.Name),
                    this.Model.Player.Name),
                new XElement(
                    nameof(this.Model.Player.Score),
                    this.Model.Player.Score),
                new XElement(
                    nameof(this.Model.Player.PositionX),
                    this.Model.Player.PositionX),
                new XElement(
                    nameof(this.Model.Player.PositionY),
                    this.Model.Player.PositionY));
            rootNode.Add(playerNode);

            // Enemy node
            XElement enemyNode = new XElement(
                "Enemy",
                new XElement(
                    nameof(this.Model.EnemyWave),
                    this.Model.EnemyWave),
                new XElement(
                    nameof(this.Model.EnemyPowerUp),
                    this.Model.EnemyPowerUp),
                new XElement(
                    nameof(this.Model.EnemyNumber),
                    this.Model.EnemyNumber));
            rootNode.Add(enemyNode);

            // Settings
            XElement settingsNode = new XElement(
                nameof(this.Model.GameSettings),
                new XElement(
                    nameof(this.Model.GameSettings.MapType),
                    this.Model.GameSettings.MapType),
                new XElement(
                    nameof(this.Model.GameSettings.Difficulty),
                    this.Model.GameSettings.Difficulty));

            XElement map = new XElement(
                nameof(this.Model.Map),
                new XAttribute("lengthX", this.Model.Map.GetLength(0)),
                new XAttribute("lengthY", this.Model.Map.GetLength(1)));
            for (int i = 0; i < this.Model.Map.GetLength(0); i++)
            {
                XElement oneRow = new XElement(
                    "Row",
                    new XAttribute("id", i));

                string mapRowValues = string.Empty;
                for (int j = 0; j < this.Model.Map.GetLength(1); j++)
                {
                    if (this.Model.Map[i, j] is GrassTile)
                    {
                        mapRowValues += "o";
                    }
                    else if (this.Model.Map[i, j] is DirtTile)
                    {
                        mapRowValues += "x";
                    }
                }

                oneRow.Value = mapRowValues;
                map.Add(oneRow);
            }

            settingsNode.Add(map);
            rootNode.Add(settingsNode);

            // Nexus
            XElement nexusNode = new XElement(
                nameof(this.Model.Nexus),
                new XElement(
                    nameof(this.Model.Nexus.PositionX),
                    this.Model.Nexus.PositionX),
                new XElement(
                    nameof(this.Model.Nexus.PositionY),
                    this.Model.Nexus.PositionY),
                new XElement(
                    nameof(this.Model.Nexus.HealtPoints),
                    this.Model.Nexus.HealtPoints));
            rootNode.Add(nexusNode);

            xdoc.Add(rootNode);
            xdoc.Save(path);
        }
    }
}
